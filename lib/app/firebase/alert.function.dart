import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_2/app/models/alert.dart';
import 'package:flutter_2/app/firebase/user.function.dart';

class AlertFunction {
  UsersFunction _usersFunction = UsersFunction();

  final CollectionReference collection =
      FirebaseFirestore.instance.collection('alert');

  Future<List<Alert>> getAlert() async {
    List<Alert> alertList = [];

    QuerySnapshot querySnapshot =
        await collection.orderBy('fecha', descending: true).get();

    if (querySnapshot.docs.length > 0) {
      for (var i = 0; i < querySnapshot.docs.length; i++) {
        QueryDocumentSnapshot<Object> user = await _usersFunction
            .getUserByEmail(querySnapshot.docs[i]['correo_electronico']);
        Alert alert = Alert(
          correoElectronico: querySnapshot.docs[i]['correo_electronico'],
          descripcion: querySnapshot.docs[i]['body'],
          titulo: querySnapshot.docs[i]['title'],
          fecha: querySnapshot.docs[i]['fecha'],
          nombre: user == null
              ? '-'
              : (user['nombres'] +
                  ' ' +
                  user['apellido_paterno'] +
                  ' ' +
                  user['apellido_materno']),
        );
        alertList.add(alert);
      }
    }

    return alertList;
  }
}
