import 'package:cloud_firestore/cloud_firestore.dart';

class VideoFunction {
  final CollectionReference collection =
      FirebaseFirestore.instance.collection('videos');

  // Future<int> getIndexMax() async {
  //   List<int> indexList = [];
  //   QuerySnapshot querySnapshot = await collection.get();

  //   if (querySnapshot.docs.length > 0) {
  //     querySnapshot.docs.forEach((doc) {
  //       indexList.add(int.parse(doc.id));
  //     });
  //   }

  //   return indexList.isEmpty ? 0 : indexList.reduce(max);
  // }

  Future<void> saveVideo(Map<String, dynamic> map) async {
    collection
        .add(map)
        .then((value) => print("Video Added"))
        .catchError((error) => print("Failed to add video: $error"));
  }

  Future<void> editVideo(Map<String, dynamic> map, String key) async {
    collection
        .doc((key).toString())
        .update(map)
        .then((value) => print("Video Edited"))
        .catchError((error) => print("Failed to user video: $error"));
  }

  Future<void> deleteVideo(String key) async {
    collection
        .doc((key).toString())
        .delete()
        .then((value) => print("Video Removed"))
        .catchError((error) => print("Failed to remove video: $error"));
  }

  // Future<void> editRetirement(Map<String, dynamic> map, String key) async {
  //   collection
  //       .doc((key).toString())
  //       .set(map)
  //       .then((value) => print("Retirement Edited"))
  //       .catchError((error) => print("Failed to add user: $error"));
  // }

  Future<List<dynamic>> getVideos() async {
    List<dynamic> videosList = [];

    QuerySnapshot querySnapshot = await collection.get();

    if (querySnapshot.docs.length > 0) {
      querySnapshot.docs.forEach((doc) {
        videosList.add(doc);
      });
    }

    return videosList;
  }
}
