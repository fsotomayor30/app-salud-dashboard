import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_2/app/models/user.dart';

class UsersFunction {
  final CollectionReference collection =
      FirebaseFirestore.instance.collection('users');

  // Future<int> getIndexMax() async {
  //   List<int> indexList = [];
  //   QuerySnapshot querySnapshot = await collection.get();

  //   if (querySnapshot.docs.length > 0) {
  //     querySnapshot.docs.forEach((doc) {
  //       indexList.add(int.parse(doc.id));
  //     });
  //   }

  //   return indexList.isEmpty ? 0 : indexList.reduce(max);
  // }

  Future<void> saveUser(Map<String, dynamic> map) async {
    collection
        .add(map)
        .then((value) => print("User Added"))
        .catchError((error) => print("Failed to add user: $error"));
  }

  // Future<void> editUser(Map<String, dynamic> map, String key) async {
  //   collection
  //       .doc((key).toString())
  //       .update(map)
  //       .then((value) => print("User Edited"))
  //       .catchError((error) => print("Failed to user symptom: $error"));
  // }

  // Future<void> editRetirement(Map<String, dynamic> map, String key) async {
  //   collection
  //       .doc((key).toString())
  //       .set(map)
  //       .then((value) => print("Retirement Edited"))
  //       .catchError((error) => print("Failed to add user: $error"));
  // }

  Future<List<Usuario>> getUsers() async {
    List<Usuario> usersList = [];

    QuerySnapshot querySnapshot = await collection.get();

    if (querySnapshot.docs.length > 0) {
      for (var i = 0; i < querySnapshot.docs.length; i++) {
        switch (querySnapshot.docs[i]['rol']) {
          case 'cuidador':
            QueryDocumentSnapshot<Object> user = await getUserByEmail(
                querySnapshot.docs[i]['correo_electronico_adulto_mayor']);
            Usuario usuario = Usuario(
              id: querySnapshot.docs[i].id,
              rol: querySnapshot.docs[i]['rol'],
              habilitado: querySnapshot.docs[i]['habilitado'],
              correoElectronico: querySnapshot.docs[i]['correo_electronico'],
              nombreAdultoMayor: user['nombres'] +
                  ' ' +
                  user['apellido_paterno'] +
                  ' ' +
                  user['apellido_materno'],
            );
            usersList.add(usuario);
            break;
          case 'adulto_mayor':
            Usuario usuario = Usuario(
                id: querySnapshot.docs[i].id,
                nombres: querySnapshot.docs[i]['nombres'],
                habilitado: querySnapshot.docs[i]['habilitado'],
                apellidoMaterno: querySnapshot.docs[i]['apellido_materno'],
                apellidoPaterno: querySnapshot.docs[i]['apellido_paterno'],
                correoElectronico: querySnapshot.docs[i]['correo_electronico'],
                direccionParticular: querySnapshot.docs[i]
                    ['direccion_particular'],
                genero: querySnapshot.docs[i]['genero'],
                rol: querySnapshot.docs[i]['rol'],
                sector: querySnapshot.docs[i]['sector']);
            usersList.add(usuario);

            break;

          case 'admin':
            Usuario usuario = Usuario(
                id: querySnapshot.docs[i].id,
                habilitado: querySnapshot.docs[i]['habilitado'],
                correoElectronico: querySnapshot.docs[i]['correo_electronico'],
                rol: querySnapshot.docs[i]['rol']);
            usersList.add(usuario);

            break;
        }
        // Usuario usuario = Usuario(
        //   apellidoMaterno: querySnapshot.docs[i].get('apellido_materno'),
        // );
        // usersList.add(usuario);
      }
    }

    return usersList;
  }

  Future<QueryDocumentSnapshot<Object>> getUserByEmail(String email) async {
    QueryDocumentSnapshot<Object> user;

    QuerySnapshot querySnapshot =
        await collection.where('correo_electronico', isEqualTo: email).get();

    if (querySnapshot.docs.length > 0) {
      user = querySnapshot.docs.first;
    }

    return user;
  }

  Future<void> edituser(Map<String, dynamic> map, String key) async {
    collection
        .doc((key).toString())
        .update(map)
        .then((value) => print("User Edited"))
        .catchError((error) => print("Failed to edit user: $error"));
  }
}
