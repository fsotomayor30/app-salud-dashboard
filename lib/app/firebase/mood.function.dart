import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_2/app/models/mood.dart';
import 'package:flutter_2/app/firebase/user.function.dart';

class MoodFunction {
  UsersFunction _usersFunction = UsersFunction();

  final CollectionReference collection =
      FirebaseFirestore.instance.collection('mood');

  // Future<int> getIndexMax() async {
  //   List<int> indexList = [];
  //   QuerySnapshot querySnapshot = await collection.get();

  //   if (querySnapshot.docs.length > 0) {
  //     querySnapshot.docs.forEach((doc) {
  //       indexList.add(int.parse(doc.id));
  //     });
  //   }

  //   return indexList.isEmpty ? 0 : indexList.reduce(max);
  // }

  // Future<void> saveSymptom(Map<String, dynamic> map) async {
  //   collection
  //       .add(map)
  //       .then((value) => print("Symptom Added"))
  //       .catchError((error) => print("Failed to add user: $error"));
  // }

  // Future<void> editSymptom(Map<String, dynamic> map, String key) async {
  //   collection
  //       .doc((key).toString())
  //       .update(map)
  //       .then((value) => print("Symptom Edited"))
  //       .catchError((error) => print("Failed to add symptom: $error"));
  // }

  Future<List<Mood>> getMood() async {
    List<Mood> moodList = [];

    QuerySnapshot querySnapshot = await collection.get();

    if (querySnapshot.docs.length > 0) {
      for (var i = 0; i < querySnapshot.docs.length; i++) {
        QueryDocumentSnapshot<Object> user =
            await _usersFunction.getUserByEmail(querySnapshot.docs[i]['user']);
        Mood measurement = Mood(
          estadoAnimo: querySnapshot.docs[i]['estado_animo'],
          fechaRegistro: querySnapshot.docs[i]['fecha_registro'],
          usuario: user == null
              ? '-'
              : (user['nombres'] +
                  ' ' +
                  user['apellido_paterno'] +
                  ' ' +
                  user['apellido_materno']),
        );

        moodList.add(measurement);
      }
    }

    return moodList;
  }

  Future<List<Mood>> getMoodByUser(String email) async {
    List<Mood> moodList = [];

    QuerySnapshot querySnapshot = await collection
        .where('user', isEqualTo: email)
        .orderBy('fecha_registro', descending: true)
        .get();

    if (querySnapshot.docs.length > 0) {
      for (var i = 0; i < querySnapshot.docs.length; i++) {
        Mood measurement = Mood(
          estadoAnimo: querySnapshot.docs[i]['estado_animo'],
          fechaRegistro: querySnapshot.docs[i]['fecha_registro'],
          usuario: querySnapshot.docs[i]['user'],
        );

        moodList.add(measurement);
      }
    }

    return moodList;
  }

  // Future<dynamic> getMySymptomByKey(String key) async {
  //   String dateNowTo = DateFormat('dd-MM-yyyy 00:00').format(DateTime.now());
  //   String dateNowFrom = DateFormat('dd-MM-yyyy 23:59').format(DateTime.now());
  //   SharedPreferences preferences = await SharedPreferences.getInstance();

  //   var email = preferences.getString('email');

  //   DocumentSnapshot documentSnapshot = await collection
  //       .doc(key)
  //       // .orderBy('fecha_registro', descending: true)
  //       .get();

  //   if (documentSnapshot.get('user') == email &&
  //       (((documentSnapshot.get('fecha_registro').compareTo(dateNowTo) == 1) &&
  //               (documentSnapshot
  //                       .get('fecha_registro')
  //                       .compareTo(dateNowFrom) ==
  //                   -1)) ||
  //           ((documentSnapshot.get('fecha_registro').compareTo(dateNowFrom) ==
  //                   1) ||
  //               (documentSnapshot.get('fecha_registro').compareTo(dateNowTo) ==
  //                   1)))) {
  //     return documentSnapshot.data();
  //   }

  //   return null;
  // }

  // Future<List<dynamic>> getMySymptomInDay() async {
  //   String dateNowTo = DateFormat('dd-MM-yyyy 00:00').format(DateTime.now());
  //   String dateNowFrom = DateFormat('dd-MM-yyyy 23:59').format(DateTime.now());

  //   SharedPreferences preferences = await SharedPreferences.getInstance();

  //   var email = preferences.getString('email');
  //   List<dynamic> symptomList = [];

  //   QuerySnapshot querySnapshot = await collection
  //       .where('user', isEqualTo: email)
  //       .where('fecha_registro', isGreaterThanOrEqualTo: dateNowTo)
  //       .where('fecha_registro', isLessThanOrEqualTo: dateNowFrom)
  //       .get();

  //   if (querySnapshot.docs.length > 0) {
  //     querySnapshot.docs.forEach((doc) {
  //       symptomList.add(doc);
  //     });
  //   }

  //   return symptomList;
  // }
}
