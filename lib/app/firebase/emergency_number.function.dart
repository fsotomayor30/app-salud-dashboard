import 'package:cloud_firestore/cloud_firestore.dart';

class EmergencyNumberFunction {
  final CollectionReference collection =
      FirebaseFirestore.instance.collection('emergency_number');

  // Future<int> getIndexMax() async {
  //   List<int> indexList = [];
  //   QuerySnapshot querySnapshot = await collection.get();

  //   if (querySnapshot.docs.length > 0) {
  //     querySnapshot.docs.forEach((doc) {
  //       indexList.add(int.parse(doc.id));
  //     });
  //   }

  //   return indexList.isEmpty ? 0 : indexList.reduce(max);
  // }

  Future<void> saveEmergencyNumber(Map<String, dynamic> map) async {
    collection
        .add(map)
        .then((value) => print("Emergency Number Added"))
        .catchError((error) => print("Failed to add emergency number: $error"));
  }

  Future<void> editEmergencyNumber(Map<String, dynamic> map, String key) async {
    collection
        .doc((key).toString())
        .update(map)
        .then((value) => print("Emergency Number Edited"))
        .catchError(
            (error) => print("Failed to edit emergency number: $error"));
  }

  Future<void> deleteEmergencyNumber(String key) async {
    collection
        .doc((key).toString())
        .delete()
        .then((value) => print("Emergency umber Removed"))
        .catchError(
            (error) => print("Failed to remove emergency number: $error"));
  }

  // Future<void> editRetirement(Map<String, dynamic> map, String key) async {
  //   collection
  //       .doc((key).toString())
  //       .set(map)
  //       .then((value) => print("Retirement Edited"))
  //       .catchError((error) => print("Failed to add user: $error"));
  // }

  Future<List<dynamic>> getEmergencyNumbers() async {
    List<dynamic> emergencyNumbersList = [];

    QuerySnapshot querySnapshot =
        await collection.where('all_user', isEqualTo: true).get();

    if (querySnapshot.docs.length > 0) {
      querySnapshot.docs.forEach((doc) {
        emergencyNumbersList.add(doc);
      });
    }

    return emergencyNumbersList;
  }
}
