import 'package:flutter_2/app/constant/constants.dart';
import 'package:jaguar_jwt/jaguar_jwt.dart';

String createJwt(String pass) {
  final JwtClaim claim = JwtClaim(subject: pass);
  final String token = issueJwtHS256(claim, SECRET_KET);
  print('HASH $token');
  return token;
}

String processingJwt(String token) {
  try {
    print(SECRET_KET);
    final JwtClaim decClaimSet = verifyJwtHS256Signature(token, SECRET_KET);
    return decClaimSet.subject;
  } on JwtException {
    print('ERROR PROCESSING JWT');
    return null;
  }
}
