import 'package:firebase_auth/firebase_auth.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<UserCredential> signInWithEmailAndPassword(
      {String email, String password}) async {
    print('LOGIN');
    return await _auth.signInWithEmailAndPassword(
        email: email, password: password);
  }

  Future<dynamic> createUserWithEmailAndPassword(
      {String email, String password}) async {
    try {
      return await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        return 'The password provided is too weak.';
      } else if (e.code == 'email-already-in-use') {
        return 'The account already exists for that email.';
      }
    } catch (e) {
      return null;
    }
  }

  Future<dynamic> recoverPassword({String email}) async {
    try {
      await FirebaseAuth.instance.sendPasswordResetEmail(email: email);
      return true;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'invalid-email') {
        return 'Correo electrónico inválido.';
      } else if (e.code == 'user-not-found') {
        return 'Usuario no encontrado.';
      }
    } catch (e) {
      return null;
    }
  }

  Future<void> signOut() async {
    await FirebaseAuth.instance.signOut();
  }
}
