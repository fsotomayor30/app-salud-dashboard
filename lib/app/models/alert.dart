class Alert {
  Alert(
      {this.nombre,
      this.correoElectronico,
      this.fecha,
      this.titulo,
      this.descripcion});

  final String nombre;
  final String correoElectronico;
  final String fecha;
  final String titulo;
  final String descripcion;
}
