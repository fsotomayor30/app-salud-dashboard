class EmergencyNumber {
  EmergencyNumber({this.id, this.nombre, this.telefono, this.visible});

  final String nombre;
  final String id;
  final String telefono;
  final bool visible;
}
