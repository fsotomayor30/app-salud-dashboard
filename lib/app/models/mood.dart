class Mood {
  Mood({this.usuario, this.estadoAnimo, this.fechaRegistro});

  final String usuario;
  final String estadoAnimo;
  final String fechaRegistro;
}
