class Usuario {
  Usuario(
      {this.id,
      this.nombres,
      this.apellidoMaterno,
      this.apellidoPaterno,
      this.genero,
      this.correoElectronico,
      this.direccionParticular,
      this.sector,
      this.rol,
      this.nombreAdultoMayor,
      this.habilitado});

  final String nombreAdultoMayor;
  final String id;
  final String nombres;
  final String apellidoMaterno;
  final String apellidoPaterno;
  final String genero;
  final String correoElectronico;
  final String direccionParticular;
  final String sector;
  final String rol;
  final bool habilitado;
}
