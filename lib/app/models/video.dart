class Video {
  Video(
      {this.id, this.duracion, this.tipo, this.titulo, this.url, this.visible});

  final String duracion;
  final String id;
  final String tipo;
  final String titulo;
  final String url;
  final bool visible;
}
