class Measurement {
  Measurement(
      {this.usuario,
      this.colesterol,
      this.glicemia,
      this.presionDiastolica,
      this.presionSistolica,
      this.fechaRegistro});

  final String usuario;
  final String colesterol;
  final String glicemia;
  final String presionDiastolica;
  final String presionSistolica;
  final String fechaRegistro;
}
