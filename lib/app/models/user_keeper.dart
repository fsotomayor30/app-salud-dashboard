class UsuarioCuidador {
  UsuarioCuidador(
      {this.correoElectronicoAdultoMayor, this.correoElectronico, this.rol});
  final String correoElectronicoAdultoMayor;
  final String correoElectronico;
  final String rol;
}
