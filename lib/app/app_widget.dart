import 'package:flutter/material.dart';
import 'package:flutter_2/app/pages/splash/splash.dart';

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'SaludAssist',
      home: Scaffold(
        // drawer: Drawer(),
        // drawerEdgeDragWidth: 200,
        body: Splash(),
      ),
    );
  }
}
