import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_2/app/firebase/user.function.dart';
import 'package:flutter_2/app/pages/home_page/home.dart';
import 'package:flutter_2/app/pages/login/login.dart';
import 'package:flutter_2/app/service_auth/auth.service.dart';
import 'package:flutter_2/app/service_jwt/jwt.service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  final AuthService authService = AuthService();
  UsersFunction usersFunction = UsersFunction();

  @override
  void initState() {
    super.initState();
    loadData();
  }

  Future<void> loadData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String dataEncrypt = preferences.getString('data');
    if (dataEncrypt == null) {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => LoginPage()));
    } else {
      String data = processingJwt(dataEncrypt);
      dynamic dataDecode = jsonDecode(data);
      try {
        await authService.signInWithEmailAndPassword(
            email: dataDecode['email'], password: dataDecode['password']);
        await checkLogin(dataDecode['email'], dataDecode['password']);
      } on FirebaseAuthException {
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => LoginPage()));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return MaterialApp(
      home: Center(
        child: Image.asset(
          'assets/logoubb.png',
          width: size.width * 0.3,
        ),
      ),
    );
  }

  Future checkLogin(String email, String contrasena) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;

    dynamic user = await usersFunction.getUserByEmail(email);
    if (user != null) {
      if (user['habilitado'] == false) {
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => LoginPage()));
      }

      if (user['rol'] != 'super_admin' && user['rol'] != 'admin') {
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => LoginPage()));
      }

      Map<String, dynamic> userEncrypt = {
        'email': email,
        'password': contrasena,
        'rol': user['rol']
      };

      preferences.setString('data', createJwt(jsonEncode(userEncrypt)));

      final gcmToken = await _firebaseMessaging.getToken();
      Map<String, dynamic> tokenRing = {'token_ring': gcmToken};
      await usersFunction.edituser(tokenRing, user.id);
      // await preferences.setString('token_ring', gcmToken);
      // setState(() {
      //   loading = false;
      // });
      // final _messaging = FBMessaging.instance;
      // _messaging.requestPermission().then((_) async {
      //   final String _token = await _messaging.getToken();
      //   print('Token: $_token');
      //   Map<String, dynamic> tokenRing = {'token_ring': _token};

      //   await usersFunction.edituser(tokenRing, user.id);
      // });

      // final gcmToken = await this._firebaseMessaging.getToken();
      // Map<String, dynamic> tokenRing = {'token_ring': gcmToken};
      // await usersFunction.editUser(tokenRing, user.id);
      // await preferences.setString('token_ring', gcmToken);

      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => Home(user: userEncrypt)));
    } else {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => LoginPage()));
    }
  }
}
