import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_2/app/components/notification_item.widget.dart';
import 'package:flutter_2/app/constant/constants.dart';
import 'package:flutter_2/app/pages/login/login.dart';
import 'package:flutter_2/app/service_auth/auth.service.dart';
import 'package:flutter_2/app/tabs/adminsTab.dart';
import 'package:flutter_2/app/tabs/alertsTab.dart';
import 'package:flutter_2/app/tabs/careTakersTab.dart';
import 'package:flutter_2/app/tabs/emergencyNumberTab.dart';
import 'package:flutter_2/app/tabs/measurementsTab.dart';
import 'package:flutter_2/app/tabs/moodsTab.dart';
import 'package:flutter_2/app/tabs/olderAdultsTab.dart';
import 'package:flutter_2/app/tabs/videosTab.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../tabs/homeTab.dart';
import 'components/side_menu.dart';

class Home extends StatefulWidget {
  final dynamic user;
  Home({@required this.user});
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  TabController _tabController;
  FToast fToast;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 9, initialIndex: 0);
    fToast = FToast();
    fToast.init(context);

    FirebaseMessaging.onMessage.listen((remoteMessage) async {
      if (remoteMessage != null) {
        print(remoteMessage.data);
        _showToast(
            remoteMessage.data['title'],
            remoteMessage.data['body'],
            remoteMessage.data['date'],
            _tabController,
            remoteMessage.data['correoElectronico'],
            context);
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        elevation: 0,
        centerTitle: true,
        title: Text(
          'Panel SaludAssist',
          style: TextStyle(
            fontSize: 15,
          ),
        ),
        actions: [
          IconButton(
              icon: Icon(Icons.settings_power),
              onPressed: () async {
                AuthService authService = AuthService();

                await authService.signOut();
                SharedPreferences preferences =
                    await SharedPreferences.getInstance();
                await preferences.clear();
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => LoginPage()));
              })
        ],
      ),
      body: ResponsiveBuilder(
        builder: (context, sizingInformation) {
          if (sizingInformation.isDesktop) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SideMenu(tabController: _tabController, usuario: widget.user),
                //RETIFICAR AQUI
                Expanded(
                  child: TabBarView(
                      physics: NeverScrollableScrollPhysics(),
                      controller: _tabController,
                      children: [
                        HomeTab(),
                        OlderAdultsTab(),
                        CaretakersTab(),
                        AdminsTab(),
                        MeasurementsTab(),
                        ModsTab(),
                        VideosTab(),
                        EmergencyNumberTab(),
                        AlertsTab()
                      ]),
                )
              ],

              //Outras Tabs
            );
          }

          if (sizingInformation.isMobile) {
            return Text("Is Mobile");
          }
          // if (sizingInformation.isTablet) {
          //   return Text("Is Tablet :)");
          // }

          return Text("Default");
        },
      ),
    );
  }

  _showToast(
      String title,
      String body,
      String fecha,
      TabController tabController,
      String correoElectronico,
      BuildContext newContext) {
    Widget toast = NotificationItemWidget(
      newContext: newContext,
      title: title,
      body: body,
      fecha: fecha,
      tabController: tabController,
      correoElectronico: correoElectronico,
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.BOTTOM_RIGHT,
      toastDuration: Duration(seconds: 5),
    );

    // Custom Toast Position
    // fToast.showToast(
    //     child: toast,
    //     toastDuration: Duration(seconds: 2),
    //     positionedToastBuilder: (context, child) {
    //       return Positioned(
    //         child: child,
    //         top: 16.0,
    //         left: 16.0,
    //       );
    //     });
  }
}

class SalesData {
  SalesData(this.year, this.sales);
  final String year;
  final double sales;
}
