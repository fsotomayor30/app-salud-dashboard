import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';

import '../../../components/colapseItem.dart';
import 'user_profile.dart';

class SideMenu extends StatefulWidget {
  const SideMenu({
    Key key,
    @required TabController tabController,
    @required dynamic usuario,
  })  : _tabController = tabController,
        _usuario = usuario,
        super(key: key);

  final TabController _tabController;
  final dynamic _usuario;

  @override
  _SideMenuState createState() => _SideMenuState();
}

class _SideMenuState extends State<SideMenu> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFF1e282c),
      width: 240,
      child: Theme(
        data: ThemeData(highlightColor: Colors.red),
        child: Scrollbar(
          child: ListView(
            children: [
              SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: UserProfile(
                  email: widget._usuario['email'],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    widget._tabController.animateTo((0));
                  });
                },
                child: CustomSideMenuItem(
                  title: 'Inicio',
                  icon: Icons.home,
                ),
              ),
              buildMenuPrincipalItens(),
              InkWell(
                child: Container(
                  color: Color(0xFF1e282c),
                  padding: const EdgeInsets.symmetric(
                    horizontal: 10,
                    vertical: 5,
                  ),
                  child: ExpandablePanel(
                      theme: ExpandableThemeData(
                        iconColor: Colors.white,
                        iconSize: 14,
                      ),
                      header: Container(
                        child: Row(
                          children: [
                            SizedBox(
                              width: 3,
                            ),
                            Icon(
                              Icons.timeline,
                              color: Colors.white,
                              size: 18,
                            ),
                            SizedBox(
                              width: 7,
                            ),
                            Text(
                              "Mediciones",
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            )
                          ],
                        ),
                      ),
                      collapsed: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 10,
                          vertical: 5,
                        ),
                        child: Column(
                          children: [
                            InkWell(
                              onTap: () {
                                setState(() {
                                  widget._tabController.animateTo((4));
                                });
                              },
                              child: ColaspeItem(
                                  label: "Mediciones diarias",
                                  icon: Icons.alarm),
                            ),
                            InkWell(
                              onTap: () {
                                setState(() {
                                  widget._tabController.animateTo((5));
                                });
                              },
                              child: ColaspeItem(
                                  label: "Estados de animo",
                                  icon: Icons.thumbs_up_down),
                            ),
                            // ColaspeItem(
                            //   label: "Acessibilidade",
                            //   icon: Icons.accessible,
                            // ),
                          ],
                        ),
                      )),
                ),
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    widget._tabController.animateTo((6));
                  });
                },
                child: CustomSideMenuItem(
                  title: 'Videos',
                  icon: Icons.play_circle_fill,
                ),
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    widget._tabController.animateTo((7));
                  });
                },
                child: CustomSideMenuItem(
                  title: 'Números de emergencia',
                  icon: Icons.phone,
                ),
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    widget._tabController.animateTo((8));
                  });
                },
                child: CustomSideMenuItem(
                  title: 'Alertas',
                  icon: Icons.warning,
                ),
              ),
              // CustomSideMenuItem(
              //   title: 'Ayuda',
              //   icon: Icons.help,
              // ),
              // CustomSideMenuItem(
              //   title: 'Sobre nosotros',
              //   icon: Icons.info,
              // )
            ],
          ),
        ),
      ),
    );
  }

  Container buildMenuPrincipalItens() {
    return Container(
      color: Color(0xFF1e282c),
      padding: const EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 5,
      ),
      child: ExpandablePanel(
          theme: ExpandableThemeData(
            iconColor: Colors.white,
            iconSize: 14,
          ),
          header: Row(
            children: [
              SizedBox(
                width: 3,
              ),
              Icon(
                Icons.people,
                color: Colors.white,
                size: 18,
              ),
              SizedBox(
                width: 7,
              ),
              Text(
                "Usuarios",
                style: TextStyle(
                  color: Colors.white,
                ),
              )
            ],
          ),
          collapsed: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 10,
              vertical: 5,
            ),
            child: Column(
              children: [
                InkWell(
                    onTap: () {
                      setState(() {
                        widget._tabController.animateTo((1));
                      });
                    },
                    child: ColaspeItem(
                        label: "Adultos mayores", icon: Icons.help)),
                InkWell(
                  onTap: () {
                    setState(() {
                      widget._tabController.animateTo((2));
                    });
                  },
                  child: ColaspeItem(
                    label: "Cuidadores",
                    icon: Icons.lock,
                  ),
                ),
                widget._usuario['rol'] == 'super_admin'
                    ? InkWell(
                        onTap: () {
                          setState(() {
                            widget._tabController.animateTo((3));
                          });
                        },
                        child: ColaspeItem(
                          label: "Administradores",
                          icon: Icons.vpn_key,
                        ),
                      )
                    : Container(),
              ],
            ),
          )),
    );
  }
}

class CustomSideMenuItem extends StatelessWidget {
  const CustomSideMenuItem({
    Key key,
    this.icon,
    this.iconSize = 18,
    this.iconColor = Colors.white,
    this.title,
    this.titleStyle,
    this.onTap,
  }) : super(key: key);

  final IconData icon;
  final double iconSize;
  final Color iconColor;

  final String title;
  final TextStyle titleStyle;

  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        height: 50,
        padding: const EdgeInsets.symmetric(horizontal: 10),
        width: double.infinity,
        color: Color(0xFF1a2226),
        child: Row(
          children: [
            Icon(
              icon,
              size: iconSize,
              color: iconColor,
            ),
            SizedBox(
              width: 4,
            ),
            Text(
              title,
              style: titleStyle ??
                  TextStyle(
                    color: Colors.grey[400],
                    fontSize: 13,
                  ),
            ),
          ],
        ),
      ),
    );
  }
}
