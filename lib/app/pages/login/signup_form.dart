import 'package:flutter/material.dart';
import 'package:flutter_2/app/constant/constants.dart';
import 'package:flutter_2/app/pages/login/login.dart';
import 'package:flutter_2/app/service_auth/auth.service.dart';
import 'package:fluttertoast/fluttertoast.dart';

class SignUp extends StatefulWidget {
  final Function onLogInSelected;

  SignUp({@required this.onLogInSelected});

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  GlobalKey<FormState> _key = GlobalKey();
  bool loading = false;
  RegExp emailRegExp =
      new RegExp(r'^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$');
  String _correo;
  final AuthService authService = AuthService();
  FToast fToast;

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Padding(
      padding: EdgeInsets.all(size.height > 770
          ? 64
          : size.height > 670
              ? 32
              : 16),
      child: Center(
        child: Card(
          elevation: 4,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(25),
            ),
          ),
          child: AnimatedContainer(
            duration: Duration(milliseconds: 200),
            height: size.height *
                (size.height > 770
                    ? 0.7
                    : size.height > 670
                        ? 0.8
                        : 0.9),
            width: 500,
            color: Colors.white,
            child: Center(
              child: loading
                  ? LinearProgressIndicator()
                  : Form(
                      key: _key,
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: EdgeInsets.all(40),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "Recuperar contraseña",
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.grey[700],
                                ),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Container(
                                width: 30,
                                child: Divider(
                                  color: kPrimaryColor,
                                  thickness: 2,
                                ),
                              ),
                              SizedBox(
                                height: 32,
                              ),
                              SizedBox(
                                height: 32,
                              ),
                              TextFormField(
                                onSaved: (text) => _correo = text,
                                keyboardType: TextInputType.emailAddress,
                                validator: (text) {
                                  if (text.length == 0) {
                                    return "Este campo correo es requerido";
                                  } else if (!emailRegExp.hasMatch(text)) {
                                    return "El formato para correo no es correcto";
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                  hintText: 'Ingrese correo',
                                  labelText: 'Correo',
                                  suffixIcon: Icon(
                                    Icons.mail_outline,
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 32,
                              ),
                              InkWell(
                                onTap: () async {
                                  if (_key.currentState.validate()) {
                                    _key.currentState.save();
                                    setState(() {
                                      loading = true;
                                    });

                                    dynamic recoverPassword =
                                        await authService.recoverPassword(
                                      email: _correo,
                                    );

                                    if (recoverPassword ==
                                        'Correo electrónico inválido.') {
                                      setState(() {
                                        loading = false;
                                      });
                                      _showToast(
                                          Colors.red,
                                          'Correo electrónico inválido',
                                          Icons.close);
                                      return;
                                    }

                                    if (recoverPassword ==
                                        'Usuario no encontrado.') {
                                      setState(() {
                                        loading = false;
                                      });
                                      _showToast(Colors.red,
                                          'Usuario no encontrado', Icons.close);
                                      return;
                                    }

                                    if (recoverPassword == null) {
                                      setState(() {
                                        loading = false;
                                      });
                                      _showToast(Colors.red,
                                          'Ha ocurrido un error', Icons.close);
                                      return;
                                    }

                                    setState(() {
                                      loading = false;
                                    });

                                    _showToast(
                                        Colors.green,
                                        'Correo de recuperación enviado',
                                        Icons.check);

                                    Navigator.of(context).pushReplacement(
                                        MaterialPageRoute(
                                            builder: (context) => LoginPage()));
                                  }
                                },
                                child: Container(
                                  height: 50,
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                    color: kPrimaryColor,
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(25),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: kPrimaryColor.withOpacity(0.2),
                                        spreadRadius: 4,
                                        blurRadius: 7,
                                        offset: Offset(0, 3),
                                      ),
                                    ],
                                  ),
                                  child: Center(
                                    child: Text(
                                      'Recuperar contraseña',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 32,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "Volver a inciar sesión",
                                    style: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 14,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      widget.onLogInSelected();
                                    },
                                    child: Row(
                                      children: [
                                        Text(
                                          "Iniciar sesión",
                                          style: TextStyle(
                                            color: kPrimaryColor,
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                        SizedBox(
                                          width: 8,
                                        ),
                                        Icon(
                                          Icons.arrow_forward,
                                          color: kPrimaryColor,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
            ),
          ),
        ),
      ),
    );
  }

  _showToast(Color color, String text, IconData icon) {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: color,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(icon),
          SizedBox(
            width: 12.0,
          ),
          Text(text),
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.BOTTOM,
      toastDuration: Duration(seconds: 2),
    );

    // Custom Toast Position
    // fToast.showToast(
    //     child: toast,
    //     toastDuration: Duration(seconds: 2),
    //     positionedToastBuilder: (context, child) {
    //       return Positioned(
    //         child: child,
    //         top: 16.0,
    //         left: 16.0,
    //       );
    //     });
  }
}
