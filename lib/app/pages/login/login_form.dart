import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_2/app/firebase/user.function.dart';
import 'package:flutter_2/app/pages/home_page/home.dart';
import 'package:flutter_2/app/constant/constants.dart';
import 'package:flutter_2/app/service_auth/auth.service.dart';
import 'package:flutter_2/app/service_jwt/jwt.service.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LogIn extends StatefulWidget {
  final Function onSignUpSelected;

  LogIn({@required this.onSignUpSelected});

  @override
  _LogInState createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  bool loading = false;
  // RegExp emailRegExp =
  //     new RegExp(r'^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$');
  RegExp contRegExp = new RegExp(r'^([1-zA-Z0-1@.\s]{1,255})$');
  String _correo;
  String _contrasena;
  GlobalKey<FormState> _key = GlobalKey();
  final AuthService authService = AuthService();
  UsersFunction usersFunction = UsersFunction();
  FToast fToast;

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Padding(
      padding: EdgeInsets.all(size.height > 770
          ? 64
          : size.height > 670
              ? 32
              : 16),
      child: Center(
        child: Card(
          elevation: 4,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(25),
            ),
          ),
          child: AnimatedContainer(
            duration: Duration(milliseconds: 200),
            height: size.height *
                (size.height > 770
                    ? 0.7
                    : size.height > 670
                        ? 0.8
                        : 0.9),
            width: 500,
            color: Colors.white,
            child: Center(
              child: loading
                  ? LinearProgressIndicator()
                  : Form(
                      key: _key,
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: EdgeInsets.all(40),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "Iniciar Sesión",
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.grey[700],
                                ),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Container(
                                width: 30,
                                child: Divider(
                                  color: kPrimaryColor,
                                  thickness: 2,
                                ),
                              ),
                              SizedBox(
                                height: 32,
                              ),
                              TextFormField(
                                onSaved: (text) => _correo = text,
                                keyboardType: TextInputType.emailAddress,
                                validator: (text) {
                                  if (text.length == 0) {
                                    return "Este campo correo es requerido";
                                  }
                                  //  else if (!emailRegExp.hasMatch(text)) {
                                  //   return "El formato para correo no es correcto";
                                  // }
                                  return null;
                                },
                                decoration: InputDecoration(
                                  hintText: 'Ingrese Correo',
                                  labelText: 'Correo',
                                  suffixIcon: Icon(
                                    Icons.mail_outline,
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 32,
                              ),
                              TextFormField(
                                onSaved: (text) => _contrasena = text,
                                keyboardType: TextInputType.text,
                                validator: (text) {
                                  if (text.length == 0) {
                                    return "Este campo contraseña es requerido";
                                  } else if (text.length <= 5) {
                                    return "Su contraseña debe ser al menos de 6 caracteres";
                                  } else if (!contRegExp.hasMatch(text)) {
                                    return "El formato para contraseña no es correcto";
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                  hintText: 'Contraseña',
                                  labelText: 'Contraseña',
                                  suffixIcon: Icon(
                                    Icons.lock_outline,
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 64,
                              ),
                              InkWell(
                                onTap: () async {
                                  if (_key.currentState.validate()) {
                                    _key.currentState.save();
                                    setState(() {
                                      loading = true;
                                    });
                                    try {
                                      await authService
                                          .signInWithEmailAndPassword(
                                              email: _correo,
                                              password: _contrasena);
                                      await checkLogin(_correo, _contrasena);
                                    } on FirebaseAuthException catch (e) {
                                      if (e.code == 'wrong-password') {
                                        setState(() {
                                          loading = false;
                                        });
                                        _showToast(Colors.red,
                                            'Usuario incorrecto', Icons.close);
                                      }

                                      if (e.code == 'user-not-found') {
                                        setState(() {
                                          loading = false;
                                        });
                                        _showToast(Colors.red,
                                            'Usuario no existe', Icons.close);
                                      }
                                    }
                                  }
                                },
                                child: Container(
                                  height: 50,
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                    color: kPrimaryColor,
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(25),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: kPrimaryColor.withOpacity(0.2),
                                        spreadRadius: 4,
                                        blurRadius: 7,
                                        offset: Offset(0, 3),
                                      ),
                                    ],
                                  ),
                                  child: Center(
                                    child: Text(
                                      'Iniciar Sesión',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 32,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "Restablecer contraseña",
                                    style: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 14,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      widget.onSignUpSelected();
                                    },
                                    child: Row(
                                      children: [
                                        Text(
                                          "Restablecer",
                                          style: TextStyle(
                                            color: kPrimaryColor,
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                        SizedBox(
                                          width: 8,
                                        ),
                                        Icon(
                                          Icons.arrow_forward,
                                          color: kPrimaryColor,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
            ),
          ),
        ),
      ),
    );
  }

  Future checkLogin(String email, String contrasena) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;

    dynamic user = await usersFunction.getUserByEmail(email);
    if (user != null) {
      if (user['habilitado'] == false) {
        _showToast(Colors.red, 'Usuario no habilitado', Icons.close);
        setState(() {
          loading = false;
        });
        return;
      }

      if (user['rol'] != 'super_admin' && user['rol'] != 'admin') {
        _showToast(
            Colors.red, 'No tienes los permisos necesarios', Icons.close);
        setState(() {
          loading = false;
        });
        return;
      }

      Map<String, dynamic> userEncrypt = {
        'email': email,
        'password': contrasena,
        'rol': user['rol']
      };

      preferences.setString('data', createJwt(jsonEncode(userEncrypt)));

      // final _messaging = FBMessaging.instance;
      // _messaging.requestPermission().then((_) async {
      //   final String _token = await _messaging.getToken();
      //   print('Token: $_token');
      //   Map<String, dynamic> tokenRing = {'token_ring': _token};

      //   await usersFunction.edituser(tokenRing, user.id);
      // });

      final gcmToken = await _firebaseMessaging.getToken();
      Map<String, dynamic> tokenRing = {'token_ring': gcmToken};
      await usersFunction.edituser(tokenRing, user.id);
      // await preferences.setString('token_ring', gcmToken);
      // setState(() {
      //   loading = false;
      // });
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => Home(user: userEncrypt)));
    } else {
      setState(() {
        loading = false;
      });
    }
  }

  _showToast(Color color, String text, IconData icon) {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: color,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(icon),
          SizedBox(
            width: 12.0,
          ),
          Text(text),
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.BOTTOM,
      toastDuration: Duration(seconds: 2),
    );

    // Custom Toast Position
    // fToast.showToast(
    //     child: toast,
    //     toastDuration: Duration(seconds: 2),
    //     positionedToastBuilder: (context, child) {
    //       return Positioned(
    //         child: child,
    //         top: 16.0,
    //         left: 16.0,
    //       );
    //     });
  }
}
