import 'package:flutter/material.dart';
import 'package:flutter_2/app/firebase/mood.function.dart';
import 'package:flutter_2/app/models/mood.dart';

class ModsTab extends StatefulWidget {
  const ModsTab({Key key}) : super(key: key);

  @override
  _ModsTabState createState() => _ModsTabState();
}

class _ModsTabState extends State<ModsTab> {
  MoodFunction moodFunction = MoodFunction();
  int _rowsPerPage = 15;
  TextEditingController _textEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: ListView(children: [
              FutureBuilder(
                future: moodFunction.getMood(),
                builder:
                    (BuildContext context, AsyncSnapshot<List<Mood>> data) {
                  if (data.hasData) {
                    return Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            controller: _textEditingController,
                            decoration: InputDecoration(
                                hintText:
                                    'Busca por Usuario/Fecha Registro/Estado de ánimo'),
                            onChanged: (String email) => setState(() {}),
                          ),
                        ),
                        PaginatedDataTable(
                            rowsPerPage: _rowsPerPage,
                            onRowsPerPageChanged: (v) {
                              setState(() {
                                _rowsPerPage = v;
                              });
                            },
                            availableRowsPerPage: [15, 30, 45, 60],
                            header: Text("Gestión de estados de ánimo"),
                            // actions: [
                            //   IconButton(icon: Icon(Icons.add), onPressed: () {}),
                            //   IconButton(icon: Icon(Icons.search), onPressed: () {})
                            // ],
                            showCheckboxColumn: true,
                            sortAscending: true,
                            columns: <DataColumn>[
                              DataColumn(label: Text("Usuario")),
                              DataColumn(label: Text("Fecha Registro")),
                              DataColumn(label: Text("Estado de animo")),
                            ],
                            source: MyDataTableSource(
                                data.data
                                    .where((element) =>
                                        element.usuario.toLowerCase().contains(
                                            _textEditingController.text
                                                .toLowerCase()) ||
                                        element.fechaRegistro
                                            .toLowerCase()
                                            .contains(_textEditingController
                                                .text
                                                .toLowerCase()) ||
                                        element.estadoAnimo
                                            .toLowerCase()
                                            .contains(_textEditingController
                                                .text
                                                .toLowerCase()))
                                    .toList(),
                                context)),
                      ],
                    );
                  }
                  return LinearProgressIndicator(
                    backgroundColor: Colors.transparent,
                    valueColor:
                        new AlwaysStoppedAnimation<Color>(Color(0xFF1e282c)),
                  );
                },
              ),
            ]),
          )
        ],
      ),
      // floatingActionButton: FloatingActionButton(
      //   backgroundColor: kPrimaryColor,
      //   onPressed: () {},
      //   child: Icon(Icons.add),
      // ),
    );
  }
}

class MyDataTableSource extends DataTableSource {
  MyDataTableSource(this.data, this.context);

  final List<dynamic> data;
  BuildContext context;
  @override
  DataRow getRow(int index) {
    if (index >= data.length) {
      return null;
    }
    return DataRow.byIndex(
      index: index,
      cells: [
        DataCell(Text('${data[index].usuario}')),
        DataCell(Text('${data[index].fechaRegistro}')),
        DataCell(Text('${data[index].estadoAnimo}')),
      ],
    );
  }

  @override
  int get selectedRowCount {
    return 0;
  }

  @override
  bool get isRowCountApproximate {
    return false;
  }

  @override
  int get rowCount {
    return data.length;
  }
}
