import 'package:flutter/material.dart';
import 'package:flutter_2/app/components/historial.widget.dart';
import 'package:flutter_2/app/firebase/alert.function.dart';

class AlertsTab extends StatefulWidget {
  const AlertsTab({Key key}) : super(key: key);

  @override
  _AlertsTabState createState() => _AlertsTabState();
}

class _AlertsTabState extends State<AlertsTab> {
  AlertFunction alertFunction = AlertFunction();
  int _rowsPerPage = 15;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: ListView(children: [
              FutureBuilder(
                future: alertFunction.getAlert(),
                builder: (BuildContext context, data) {
                  if (data.hasData) {
                    return PaginatedDataTable(
                        rowsPerPage: _rowsPerPage,
                        onRowsPerPageChanged: (v) {
                          setState(() {
                            _rowsPerPage = v;
                          });
                        },
                        availableRowsPerPage: [15, 30, 45, 60],
                        header: Text("Listado de alertas"),
                        // actions: [
                        //   IconButton(icon: Icon(Icons.add), onPressed: () {}),
                        //   IconButton(icon: Icon(Icons.search), onPressed: () {})
                        // ],
                        showCheckboxColumn: true,
                        sortAscending: true,
                        columns: <DataColumn>[
                          DataColumn(label: Text("Usuario")),
                          DataColumn(label: Text("Fecha")),
                          DataColumn(label: Text("Titulo")),
                          DataColumn(label: Text("Descripción")),
                          DataColumn(label: Text("Acciones")),
                        ],
                        source: MyDataTableSource(data.data, context));
                  }
                  return LinearProgressIndicator(
                    backgroundColor: Colors.transparent,
                    valueColor:
                        new AlwaysStoppedAnimation<Color>(Color(0xFF1e282c)),
                  );
                },
              ),
            ]),
          )
        ],
      ),
      // floatingActionButton: FloatingActionButton(
      //   backgroundColor: kPrimaryColor,
      //   onPressed: () {},
      //   child: Icon(Icons.add),
      // ),
    );
  }
}

class MyDataTableSource extends DataTableSource {
  MyDataTableSource(this.data, this.context);

  final List<dynamic> data;
  BuildContext context;
  @override
  DataRow getRow(int index) {
    if (index >= data.length) {
      return null;
    }
    return DataRow.byIndex(
      index: index,
      cells: [
        DataCell(Text('${data[index].nombre}')),
        DataCell(Text('${data[index].fecha}')),
        DataCell(Text('${data[index].titulo}')),
        DataCell(Text('${data[index].descripcion}')),
        DataCell(Row(
          children: [
            ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                primary: Colors.green,
              ),
              icon: Icon(Icons.visibility),
              onPressed: () async {
                await showDialog(
                    context: context,
                    builder: (_) {
                      return AlertDialog(
                        content: HistorialWidget(
                            email: data[index].correoElectronico),
                        actions: [
                          ElevatedButton.icon(
                            style: ElevatedButton.styleFrom(
                              primary: Colors.green,
                            ),
                            icon: Icon(Icons.west),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            label: Text("Cerrar"),
                          ),
                        ],
                      );
                    });
              },
              label: Text('Historial'),
            ),

            // SizedBox(
            //   width: 6,
            // ),
            // ElevatedButton.icon(
            //   style: ElevatedButton.styleFrom(
            //     primary: Colors.green,
            //   ),
            //   icon: Icon(Icons.visibility),
            //   onPressed: () {},
            //   label: Text("Ver"),
            // ),
          ],
        )),
      ],
    );
  }

  @override
  int get selectedRowCount {
    return 0;
  }

  @override
  bool get isRowCountApproximate {
    return false;
  }

  @override
  int get rowCount {
    return data.length;
  }
}
