import 'package:flutter/material.dart';
import 'package:flutter_2/app/components/historial.widget.dart';
import 'package:flutter_2/app/components/older_adult_form.widget.dart';
import 'package:flutter_2/app/firebase/user.function.dart';
import 'package:flutter_2/app/models/user.dart';
import 'package:url_launcher/url_launcher.dart';

class OlderAdultsTab extends StatefulWidget {
  const OlderAdultsTab({Key key}) : super(key: key);

  @override
  _OlderAdultsTabState createState() => _OlderAdultsTabState();
}

class _OlderAdultsTabState extends State<OlderAdultsTab>
    with TickerProviderStateMixin {
  UsersFunction usersFunction = UsersFunction();
  int _rowsPerPage = 15;
  TextEditingController _textEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: ListView(children: [
              FutureBuilder<List<Usuario>>(
                future: usersFunction.getUsers(),
                builder:
                    (BuildContext context, AsyncSnapshot<List<Usuario>> data) {
                  if (data.hasData) {
                    data.data
                        .where((element) => element.rol == 'adulto_mayor')
                        .toList()
                        .sort((a, b) =>
                            a.apellidoPaterno.compareTo(b.apellidoPaterno));

                    return Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            controller: _textEditingController,
                            decoration: InputDecoration(
                                hintText:
                                    'Busca por Nombre/Apellidos/Correo electrónico/Dirección/Sector'),
                            onChanged: (String email) => setState(() {}),
                          ),
                        ),
                        PaginatedDataTable(
                            rowsPerPage: _rowsPerPage,
                            onRowsPerPageChanged: (v) {
                              setState(() {
                                _rowsPerPage = v;
                              });
                            },
                            availableRowsPerPage: [15, 30, 45, 60],
                            header: Text("Gestión de adultos mayores"),
                            showCheckboxColumn: true,
                            sortAscending: true,
                            columns: <DataColumn>[
                              DataColumn(label: Text("Nombre")),
                              DataColumn(
                                label: Text("Apellido Paterno"),
                              ),
                              DataColumn(label: Text("Apellido Materno")),
                              DataColumn(label: Text("Correo electrónico")),
                              DataColumn(label: Text("Dirección")),
                              DataColumn(label: Text("Sector")),
                              DataColumn(label: Text("Habilitado")),
                              DataColumn(label: Text("Acciones")),
                            ],
                            source: MyDataTableSource(
                                data.data
                                    .where((element) =>
                                        element.rol == 'adulto_mayor' &&
                                        (element.correoElectronico.toLowerCase().contains(_textEditingController.text.toLowerCase()) ||
                                            element.nombres.toLowerCase().contains(
                                                _textEditingController.text
                                                    .toLowerCase()) ||
                                            element.apellidoMaterno
                                                .toLowerCase()
                                                .contains(_textEditingController
                                                    .text
                                                    .toLowerCase()) ||
                                            element.apellidoPaterno
                                                .toLowerCase()
                                                .contains(_textEditingController
                                                    .text
                                                    .toLowerCase()) ||
                                            element.direccionParticular
                                                .toLowerCase()
                                                .contains(
                                                    _textEditingController.text.toLowerCase()) ||
                                            element.sector.toLowerCase().contains(_textEditingController.text.toLowerCase())))
                                    .toList(),
                                context, () {
                              setState(() {});
                            })),
                      ],
                    );
                  }
                  return LinearProgressIndicator(
                    backgroundColor: Colors.transparent,
                    valueColor:
                        new AlwaysStoppedAnimation<Color>(Color(0xFF1e282c)),
                  );
                },
              ),
            ]),
          )
        ],
      ),
    );
  }
}

class MyDataTableSource extends DataTableSource {
  MyDataTableSource(this.data, this.context, this.refresh);

  final List<dynamic> data;
  BuildContext context;
  Function refresh;

  @override
  DataRow getRow(int index) {
    if (index >= data.length) {
      return null;
    }
    return DataRow.byIndex(
      index: index,
      cells: [
        DataCell(Text('${data[index].nombres}')),
        DataCell(Text('${data[index].apellidoPaterno}')),
        DataCell(Text('${data[index].apellidoMaterno}')),
        DataCell(Text('${data[index].correoElectronico}')),
        DataCell(Text('${data[index].direccionParticular}')),
        DataCell(Text('${data[index].sector}')),
        DataCell(
          Chip(
            padding: EdgeInsets.all(0),
            backgroundColor: data[index].habilitado ? Colors.green : Colors.red,
            label: Text(data[index].habilitado ? 'HABILITADO' : 'DESHABILITADO',
                style: TextStyle(color: Colors.white)),
          ),
        ),
        DataCell(Row(
          children: [
            ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                primary: data[index].habilitado ? Colors.red : Colors.green,
              ),
              icon: Icon(data[index].habilitado ? Icons.delete : Icons.done),
              onPressed: () async {
                await showDialog(
                    context: context,
                    builder: (_) {
                      return AlertDialog(
                          title: Text(data[index].habilitado
                              ? "Desea deshabilitar?"
                              : "Desea habilitar ?"),
                          content: Container(
                            height: 70,
                            child: Column(
                              children: [
                                Text(data[index].habilitado
                                    ? "Confirme para deshabilitar adulto mayor"
                                    : "Confirme para habilitar adulto mayor"),
                                SizedBox(
                                  height: 20,
                                ),
                                ElevatedButton.icon(
                                    icon: Icon(data[index].habilitado
                                        ? Icons.delete
                                        : Icons.done),
                                    style: ElevatedButton.styleFrom(
                                        primary: data[index].habilitado
                                            ? Colors.red
                                            : Colors.green),
                                    onPressed: () async {
                                      UsersFunction usersFunction =
                                          UsersFunction();

                                      Map<String, dynamic> user = {
                                        'habilitado': !data[index].habilitado
                                      };

                                      await usersFunction.edituser(
                                          user, data[index].id);

                                      Navigator.pop(context);
                                    },
                                    label: Text(data[index].habilitado
                                        ? "Deshabilitar adulto mayor"
                                        : "Habilitar adulto mayor"))
                              ],
                            ),
                          ));
                    }).then((value) {
                  refresh();
                });
              },
              label:
                  Text(data[index].habilitado ? "Deshabilitar" : 'Habilitar'),
            ),
            SizedBox(
              width: 6,
            ),
            ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                primary: Colors.blue,
              ),
              icon: Icon(Icons.edit),
              onPressed: () async {
                await showDialog(
                    context: context,
                    builder: (_) {
                      return AlertDialog(
                        title: Text("Editar adulto mayor"),
                        content: Container(
                            child: OlderAdultFormWidget(
                          id: data[index].id,
                          apellidoMaterno: data[index].apellidoMaterno,
                          apellidopaterno: data[index].apellidoPaterno,
                          direccion: data[index].direccionParticular,
                          genero: data[index].genero,
                          nombres: data[index].nombres,
                          sector: data[index].sector,
                        )),
                      );
                    }).then((value) {
                  refresh();
                });
              },
              label: Text("Editar"),
            ),
            SizedBox(
              width: 6,
            ),
            ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                primary: Colors.green,
              ),
              icon: Icon(Icons.visibility),
              onPressed: () async {
                await showDialog(
                    context: context,
                    builder: (_) {
                      return AlertDialog(
                        content: HistorialWidget(
                            email: data[index].correoElectronico),
                        actions: [
                          ElevatedButton.icon(
                            style: ElevatedButton.styleFrom(
                              primary: Colors.green,
                            ),
                            icon: Icon(Icons.west),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            label: Text("Cerrar"),
                          ),
                        ],
                      );
                    });
              },
              label: Text("Historial"),
            ),
            SizedBox(
              width: 6,
            ),
            ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                primary: Colors.deepPurple,
              ),
              icon: Icon(Icons.place),
              onPressed: () async {
                // var size = MediaQuery.of(context).size;
                // WebViewXController webviewController;

                try {
                  // MapboxGeocoding geocoding = MapboxGeocoding(
                  //     'pk.eyJ1IjoiYXBwc2FsdWQiLCJhIjoiY2txY3R1ejk4MGRvYzJ1bWl2bW00dmVhbiJ9.Gf_WCZsMYU3-xg0A8qLJBA');

                  // ForwardGeocoding forwardModel = await geocoding
                  //     .forwardModel(data[index].direccionParticular);

                  if (await canLaunch('https://www.google.cl/maps/search/' +
                      data[index].direccionParticular)) {
                    await launch('https://www.google.cl/maps/search/' +
                        data[index].direccionParticular);
                  } else {
                    throw 'Could not launch https://www.google.cl/maps/search/' +
                        data[index].direccionParticular;
                  }

                  // showDialog(
                  //     context: context,
                  //     builder: (_) {
                  //       return AlertDialog(
                  //         // title: Text("Desea eliminar ?"),
                  //         content: Container(
                  //             width: size.width * .5,
                  //             child: WebViewX(
                  //               initialContent:
                  //                   'https://www.google.cl/maps/place/Chillan,+Chill%C3%A1n,+%C3%91uble/',
                  //               initialSourceType: SourceType.URL,
                  //               onWebViewCreated: (controller) =>
                  //                   webviewController = controller,
                  //             )
                  //             // MarkersPage(
                  //             //   marker: LatLng(forwardModel.features[0].center[1],
                  //             //       forwardModel.features[0].center[0]),
                  //             // ),
                  //             ),
                  //         actions: [
                  //           ElevatedButton.icon(
                  //             style: ElevatedButton.styleFrom(
                  //               primary: Colors.green,
                  //             ),
                  //             icon: Icon(Icons.west),
                  //             onPressed: () {
                  //               Navigator.pop(context);
                  //             },
                  //             label: Text("Cerrar"),
                  //           ),
                  //         ],
                  //       );
                  //     });
                } catch (Excepetion) {
                  print(Excepetion.toString());

                  return 'Forward Geocoding Error';
                }
              },
              label: Text("Ubicación"),
            ),
          ],
        )),
      ],
    );
  }

  @override
  int get selectedRowCount {
    return 0;
  }

  @override
  bool get isRowCountApproximate {
    return false;
  }

  @override
  int get rowCount {
    return data.length;
  }
}
