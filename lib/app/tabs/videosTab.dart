import 'package:flutter/material.dart';
import 'package:flutter_2/app/components/video_form.widget.dart';
import 'package:flutter_2/app/constant/constants.dart';
import 'package:flutter_2/app/models/video.dart';
import 'package:flutter_2/app/firebase/video.function.dart';
import 'package:url_launcher/url_launcher.dart';

class VideosTab extends StatefulWidget {
  @override
  _VideosTabState createState() => _VideosTabState();
}

class _VideosTabState extends State<VideosTab> {
  VideoFunction videoFunction = VideoFunction();
  int _rowsPerPage = 15;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: ListView(children: [
              FutureBuilder(
                future: videoFunction.getVideos(),
                builder: (BuildContext context, data) {
                  if (data.hasData) {
                    return PaginatedDataTable(
                        rowsPerPage: _rowsPerPage,
                        onRowsPerPageChanged: (v) {
                          setState(() {
                            _rowsPerPage = v;
                          });
                        },
                        availableRowsPerPage: [15, 30, 45, 60],
                        header: Text("Gestión de videos"),
                        actions: [
                          FloatingActionButton(
                            backgroundColor: kPrimaryColor,
                            onPressed: () async {
                              await showDialog(
                                  context: context,
                                  builder: (_) {
                                    return AlertDialog(
                                      title: Text("Crear video"),
                                      content: Container(
                                          child: VideoFormWidget(
                                        id: null,
                                        url: '',
                                        duracion: '',
                                        tipo: '',
                                        titulo: '',
                                      )),
                                    );
                                  }).then((value) {
                                setState(() {});
                              });
                            },
                            child: Icon(Icons.add, color: Colors.white),
                          ),
                          // IconButton(icon: Icon(Icons.search), onPressed: () {})
                        ],
                        showCheckboxColumn: true,
                        sortAscending: true,
                        columns: <DataColumn>[
                          DataColumn(label: Text("Url")),
                          DataColumn(label: Text("Título")),
                          DataColumn(label: Text("Duración")),
                          DataColumn(label: Text("Tipo")),
                          DataColumn(label: Text("Visible")),
                          DataColumn(label: Text("Acciones")),
                        ],
                        source: MyDataTableSource(
                            data.data
                                .map((element) => Video(
                                    id: element.id,
                                    duracion: element['duracion'],
                                    tipo: element['tipo'],
                                    titulo: element['titulo'],
                                    url: element['url'],
                                    visible: element['visible']))
                                .toList(),
                            context, () {
                          setState(() {});
                        }));
                  }
                  return LinearProgressIndicator(
                    backgroundColor: Colors.transparent,
                    valueColor:
                        new AlwaysStoppedAnimation<Color>(Color(0xFF1e282c)),
                  );
                },
              ),
            ]),
          )
        ],
      ),
      // floatingActionButton: FloatingActionButton(
      //   backgroundColor: kPrimaryColor,
      //   onPressed: () async {
      //     await showDialog(
      //         context: context,
      //         builder: (_) {
      //           return AlertDialog(
      //             title: Text("Crear video"),
      //             content: Container(
      //                 child: VideoFormWidget(
      //               id: null,
      //               url: '',
      //               duracion: '',
      //               tipo: '',
      //               titulo: '',
      //             )),
      //           );
      //         }).then((value) {
      //       setState(() {});
      //     });
      //   },
      //   child: Icon(Icons.add),
      // ),
    );
  }
}

class MyDataTableSource extends DataTableSource {
  MyDataTableSource(this.data, this.context, this.refresh);

  final List<dynamic> data;
  BuildContext context;
  Function refresh;
  VideoFunction videoFunction = VideoFunction();

  @override
  DataRow getRow(int index) {
    if (index >= data.length) {
      return null;
    }
    return DataRow.byIndex(
      index: index,
      cells: [
        DataCell(Text('${data[index].url}')),
        DataCell(Text('${data[index].titulo}')),
        DataCell(Text('${data[index].duracion}')),
        DataCell(Text('${data[index].tipo}')),
        DataCell(
          Chip(
            padding: EdgeInsets.all(0),
            backgroundColor: data[index].visible ? Colors.green : Colors.red,
            label: Text(data[index].visible ? 'VISIBLE' : 'NO VISIBLE',
                style: TextStyle(color: Colors.white)),
          ),
        ),
        DataCell(Row(
          children: [
            ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                primary: data[index].visible ? Colors.red : Colors.green,
              ),
              icon: Icon(data[index].visible ? Icons.delete : Icons.done),
              onPressed: () async {
                await showDialog(
                    context: context,
                    builder: (_) {
                      return AlertDialog(
                          title: Text(data[index].visible
                              ? "Desea poner como no visible?"
                              : "Desea poner como visible ?"),
                          content: Container(
                            height: 70,
                            child: Column(
                              children: [
                                Text(data[index].visible
                                    ? "Confirme para poner video como no visible"
                                    : "Confirme para poner video como visible"),
                                SizedBox(
                                  height: 20,
                                ),
                                ElevatedButton.icon(
                                    icon: Icon(data[index].visible
                                        ? Icons.delete
                                        : Icons.done),
                                    style: ElevatedButton.styleFrom(
                                        primary: data[index].visible
                                            ? Colors.red
                                            : Colors.green),
                                    onPressed: () async {
                                      VideoFunction videoFunction =
                                          VideoFunction();

                                      Map<String, dynamic> user = {
                                        'visible': !data[index].visible
                                      };

                                      await videoFunction.editVideo(
                                          user, data[index].id);

                                      Navigator.pop(context);
                                    },
                                    label: Text(data[index].visible
                                        ? "No visible"
                                        : "Visible"))
                              ],
                            ),
                          ));
                    }).then((value) {
                  refresh();
                });
              },
              label: Text(data[index].visible ? "No visible" : 'Visible'),
            ),
            // DataCell(Row(
            //   children: [
            //     ElevatedButton.icon(
            //       style: ElevatedButton.styleFrom(
            //         primary: Colors.red,
            //       ),
            //       icon: Icon(Icons.delete),
            //       onPressed: () {
            //         showDialog(
            //             context: context,
            //             builder: (_) {
            //               return AlertDialog(
            //                   title: Text("Desea eliminar ?"),
            //                   content: Container(
            //                     height: 70,
            //                     child: Column(
            //                       children: [
            //                         Text("Confirme para eliminar video"),
            //                         SizedBox(
            //                           height: 20,
            //                         ),
            //                         ElevatedButton.icon(
            //                             icon: Icon(Icons.delete),
            //                             style: ElevatedButton.styleFrom(
            //                                 primary: Colors.red),
            //                             onPressed: () async {
            //                               await videoFunction
            //                                   .deleteVideo(data[index].id);
            //                               refresh();
            //                               Navigator.pop(context);
            //                             },
            //                             label:
            //                                 Text("Confirmar eliminación de video"))
            //                       ],
            //                     ),
            //                   ));
            //             });
            //       },
            //       label: Text("Eliminar"),
            //     ),
            SizedBox(
              width: 6,
            ),
            ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                primary: Colors.blue,
              ),
              icon: Icon(Icons.edit),
              onPressed: () async {
                await showDialog(
                    context: context,
                    builder: (_) {
                      return AlertDialog(
                        title: Text("Editar video"),
                        content: Container(
                            child: VideoFormWidget(
                          id: data[index].id,
                          url: data[index].url,
                          duracion: data[index].duracion,
                          tipo: data[index].tipo,
                          titulo: data[index].titulo,
                        )),
                      );
                    }).then((value) {
                  refresh();
                });
              },
              label: Text("Editar"),
            ),
            SizedBox(
              width: 6,
            ),
            ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                primary: Colors.green,
              ),
              icon: Icon(Icons.visibility),
              onPressed: () async {
                if (await canLaunch(data[index].url)) {
                  await launch(data[index].url);
                } else {
                  throw 'Could not launch ' + data[index].url;
                }
              },
              label: Text("Ver"),
            ),
          ],
        )),
      ],
    );
  }

  @override
  int get selectedRowCount {
    return 0;
  }

  @override
  bool get isRowCountApproximate {
    return false;
  }

  @override
  int get rowCount {
    return data.length;
  }
}
