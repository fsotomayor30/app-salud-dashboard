import 'package:flutter/material.dart';
import 'package:flutter_2/app/firebase/measurement.function.dart';

class MeasurementsTab extends StatefulWidget {
  const MeasurementsTab({Key key}) : super(key: key);

  @override
  _MeasurementsTabState createState() => _MeasurementsTabState();
}

class _MeasurementsTabState extends State<MeasurementsTab> {
  MeasurementFunction measurementFunction = MeasurementFunction();
  int _rowsPerPage = 15;
  TextEditingController _textEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: ListView(children: [
              FutureBuilder(
                future: measurementFunction.getSymptoms(),
                builder: (BuildContext context, data) {
                  if (data.hasData) {
                    return Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            controller: _textEditingController,
                            decoration: InputDecoration(
                                hintText:
                                    'Busca por Usuario/Fecha Registro/Colesterol/Glicemia/Presión'),
                            onChanged: (String email) => setState(() {}),
                          ),
                        ),
                        PaginatedDataTable(
                            rowsPerPage: _rowsPerPage,
                            onRowsPerPageChanged: (v) {
                              setState(() {
                                _rowsPerPage = v;
                              });
                            },
                            availableRowsPerPage: [15, 30, 45, 60],
                            header: Text("Gestión de mediciones diarias"),
                            showCheckboxColumn: true,
                            sortAscending: true,
                            columns: <DataColumn>[
                              DataColumn(label: Text("Usuario")),
                              DataColumn(label: Text("Fecha Registro")),
                              DataColumn(label: Text("Colesterol")),
                              DataColumn(label: Text("Glicemia")),
                              DataColumn(label: Text("Presión Sistólica")),
                              DataColumn(label: Text("Presión Diastólica")),
                            ],
                            source: MyDataTableSource(
                                data.data
                                    .where((element) =>
                                        element.usuario.toLowerCase().contains(_textEditingController.text.toLowerCase()) ||
                                        element.fechaRegistro
                                            .toLowerCase()
                                            .contains(_textEditingController.text
                                                .toLowerCase()) ||
                                        element.colesterol
                                            .toLowerCase()
                                            .contains(_textEditingController.text
                                                .toLowerCase()) ||
                                        element.glicemia.toLowerCase().contains(
                                            _textEditingController.text
                                                .toLowerCase()) ||
                                        element.presionSistolica
                                            .toLowerCase()
                                            .contains(_textEditingController
                                                .text
                                                .toLowerCase()) ||
                                        element.presionDiastolica
                                            .toLowerCase()
                                            .contains(_textEditingController.text.toLowerCase()))
                                    .toList(),
                                context)),
                      ],
                    );
                  }
                  return LinearProgressIndicator(
                    backgroundColor: Colors.transparent,
                    valueColor:
                        new AlwaysStoppedAnimation<Color>(Color(0xFF1e282c)),
                  );
                },
              ),
            ]),
          )
        ],
      ),
    );
  }
}

class MyDataTableSource extends DataTableSource {
  MyDataTableSource(this.data, this.context);

  final List<dynamic> data;
  BuildContext context;
  @override
  DataRow getRow(int index) {
    if (index >= data.length) {
      return null;
    }
    return DataRow.byIndex(
      index: index,
      cells: [
        DataCell(Text('${data[index].usuario}')),
        DataCell(Text('${data[index].fechaRegistro}')),
        DataCell(Text('${data[index].colesterol}')),
        DataCell(Text('${data[index].glicemia} mg/DL')),
        DataCell(Text('${data[index].presionSistolica}')),
        DataCell(Text('${data[index].presionDiastolica}')),
        // DataCell(Row(
        //   children: [
        //     ElevatedButton.icon(
        //       style: ElevatedButton.styleFrom(
        //         primary: Colors.red,
        //       ),
        //       icon: Icon(Icons.delete),
        //       onPressed: () {
        //         showDialog(
        //             context: context,
        //             builder: (_) {
        //               return AlertDialog(
        //                   title: Text("Desea eliminar ?"),
        //                   content: Container(
        //                     height: 70,
        //                     child: Column(
        //                       children: [
        //                         Text("Confirme para eliminar usuario"),
        //                         SizedBox(
        //                           height: 20,
        //                         ),
        //                         ElevatedButton.icon(
        //                             icon: Icon(Icons.delete),
        //                             style: ElevatedButton.styleFrom(
        //                                 primary: Colors.red),
        //                             onPressed: () {},
        //                             label: Text("Eliminar usuario"))
        //                       ],
        //                     ),
        //                   ));
        //             });
        //       },
        //       label: Text("Eliminar"),
        //     ),
        //     SizedBox(
        //       width: 6,
        //     ),
        //     ElevatedButton.icon(
        //       style: ElevatedButton.styleFrom(
        //         primary: Colors.blue,
        //       ),
        //       icon: Icon(Icons.edit),
        //       onPressed: () {},
        //       label: Text("Editar"),
        //     ),
        //     SizedBox(
        //       width: 6,
        //     ),
        //     ElevatedButton.icon(
        //       style: ElevatedButton.styleFrom(
        //         primary: Colors.green,
        //       ),
        //       icon: Icon(Icons.visibility),
        //       onPressed: () {},
        //       label: Text("Ver"),
        //     ),
        //   ],
        // )),
      ],
    );
  }

  @override
  int get selectedRowCount {
    return 0;
  }

  @override
  bool get isRowCountApproximate {
    return false;
  }

  @override
  int get rowCount {
    return data.length;
  }
}
