import 'package:flutter/material.dart';
import 'package:flutter_2/app/constant/constants.dart';
import 'package:flutter_2/app/firebase/measurement.function.dart';
import 'package:flutter_2/app/firebase/mood.function.dart';
import 'package:flutter_2/app/firebase/user.function.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../components/dashboardCard.dart';
import '../pages/home_page/home.dart';

class HomeTab extends StatefulWidget {
  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {
  UsersFunction usersFunction = UsersFunction();
  MeasurementFunction measurementFunction = MeasurementFunction();
  MoodFunction moodFunction = MoodFunction();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                "Panel de control",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                ),
              ),
              SizedBox(
                width: 6,
              ),
              Text(
                "Version 1.0",
                style: TextStyle(
                  fontWeight: FontWeight.w300,
                  fontSize: 11,
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FutureBuilder(
                future: usersFunction.getUsers(),
                builder: (BuildContext context, data) {
                  if (data.hasData) {
                    return Dashboardcard(
                      icon: Icons.help,
                      color: Colors.yellow[700],
                      label: "ADULTOS MAYORES",
                      value: data.data
                          .where((element) => element.rol == 'adulto_mayor')
                          .toList()
                          .length,
                    );
                  }
                  return Container(
                      height: 90,
                      width: 250,
                      child: Center(child: CircularProgressIndicator()));
                },
              ),
              FutureBuilder(
                future: usersFunction.getUsers(),
                builder: (BuildContext context, data) {
                  if (data.hasData) {
                    return Dashboardcard(
                      icon: Icons.lock,
                      color: Colors.blue,
                      label: "CUIDADORES",
                      value: data.data
                          .where((element) => element.rol == 'cuidador')
                          .toList()
                          .length,
                    );
                  }
                  return Container(
                      height: 90,
                      width: 250,
                      child: Center(child: CircularProgressIndicator()));
                },
              ),
              FutureBuilder(
                future: measurementFunction.getSymptoms(),
                builder: (BuildContext context, data) {
                  if (data.hasData) {
                    return Dashboardcard(
                      icon: Icons.timeline,
                      color: Colors.green,
                      label: "MEDICIONES DIARIAS",
                      value: data.data.length,
                    );
                  }
                  return Container(
                      height: 90,
                      width: 250,
                      child: Center(child: CircularProgressIndicator()));
                },
              ),
              FutureBuilder(
                future: moodFunction.getMood(),
                builder: (BuildContext context, data) {
                  if (data.hasData) {
                    return Dashboardcard(
                      icon: Icons.thumbs_up_down,
                      color: kPrimaryColor,
                      label: "ESTADOS DE ANIMOS",
                      value: data.data.length,
                    );
                  }
                  return Container(
                      height: 90,
                      width: 250,
                      child: Center(child: CircularProgressIndicator()));
                },
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20),
            child: Text("ESTATÍSTICAS DE USUÁRIOS"),
          ),
          Container(
              child: SfCartesianChart(
                  // Initialize category axis
                  primaryXAxis: CategoryAxis(),
                  series: <LineSeries<SalesData, String>>[
                LineSeries<SalesData, String>(
                    // Bind data source
                    dataSource: <SalesData>[
                      SalesData('Jan', 35),
                      SalesData('Feb', 208),
                      SalesData('Mar', 34),
                      SalesData('Apr', 320),
                      SalesData('May', 410)
                    ],
                    xValueMapper: (SalesData sales, _) => sales.year,
                    yValueMapper: (SalesData sales, _) => sales.sales)
              ]))
        ],
      ),
    );
  }
}
