import 'package:flutter/material.dart';
import 'package:flutter_2/app/components/emergency_number_form.widget.dart';
import 'package:flutter_2/app/constant/constants.dart';
import 'package:flutter_2/app/models/emergency_number.dart';
import 'package:flutter_2/app/firebase/emergency_number.function.dart';

class EmergencyNumberTab extends StatefulWidget {
  @override
  _EmergencyNumberTabState createState() => _EmergencyNumberTabState();
}

class _EmergencyNumberTabState extends State<EmergencyNumberTab> {
  EmergencyNumberFunction emergencyNumberFunction = EmergencyNumberFunction();
  int _rowsPerPage = 15;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: ListView(children: [
              FutureBuilder(
                future: emergencyNumberFunction.getEmergencyNumbers(),
                builder: (BuildContext context, data) {
                  if (data.hasData) {
                    return PaginatedDataTable(
                        rowsPerPage: _rowsPerPage,
                        onRowsPerPageChanged: (v) {
                          setState(() {
                            _rowsPerPage = v;
                          });
                        },
                        availableRowsPerPage: [15, 30, 45, 60],
                        header: Text("Gestión de números de emergencia"),
                        actions: [
                          FloatingActionButton(
                            backgroundColor: kPrimaryColor,
                            onPressed: () async {
                              await showDialog(
                                  context: context,
                                  builder: (_) {
                                    return AlertDialog(
                                      title: Text("Crear número de emergencia"),
                                      content: Container(
                                          child: EmergencyNumbereoFormWidget(
                                        id: null,
                                        nombre: '',
                                        telefono: '',
                                      )),
                                    );
                                  }).then((value) {
                                setState(() {});
                              });
                            },
                            child: Icon(Icons.add, color: Colors.white),
                          ),
                          // IconButton(icon: Icon(Icons.search), onPressed: () {})
                        ],
                        showCheckboxColumn: true,
                        sortAscending: true,
                        columns: <DataColumn>[
                          DataColumn(label: Text("Nombre")),
                          DataColumn(label: Text("Número")),
                          DataColumn(label: Text("Visible")),
                          DataColumn(label: Text("Acciones")),
                        ],
                        source: MyDataTableSource(
                            data.data
                                .map((element) => EmergencyNumber(
                                    id: element.id,
                                    nombre: element['nombre'],
                                    telefono: element['telefono'],
                                    visible: element['visible']))
                                .toList(),
                            context, () {
                          setState(() {});
                        }));
                  }
                  return LinearProgressIndicator(
                    backgroundColor: Colors.transparent,
                    valueColor:
                        new AlwaysStoppedAnimation<Color>(Color(0xFF1e282c)),
                  );
                },
              ),
            ]),
          )
        ],
      ),
      // floatingActionButton: FloatingActionButton(
      //   backgroundColor: kPrimaryColor,
      //   onPressed: () async {
      //     await showDialog(
      //         context: context,
      //         builder: (_) {
      //           return AlertDialog(
      //             title: Text("Crear video"),
      //             content: Container(
      //                 child: VideoFormWidget(
      //               id: null,
      //               url: '',
      //               duracion: '',
      //               tipo: '',
      //               titulo: '',
      //             )),
      //           );
      //         }).then((value) {
      //       setState(() {});
      //     });
      //   },
      //   child: Icon(Icons.add),
      // ),
    );
  }
}

class MyDataTableSource extends DataTableSource {
  MyDataTableSource(this.data, this.context, this.refresh);

  final List<dynamic> data;
  BuildContext context;
  Function refresh;
  EmergencyNumberFunction emergencyNumberFunction = EmergencyNumberFunction();

  @override
  DataRow getRow(int index) {
    if (index >= data.length) {
      return null;
    }
    return DataRow.byIndex(
      index: index,
      cells: [
        DataCell(Text('${data[index].nombre}')),
        DataCell(Text('${data[index].telefono}')),
        DataCell(
          Chip(
            padding: EdgeInsets.all(0),
            backgroundColor: data[index].visible ? Colors.green : Colors.red,
            label: Text(data[index].visible ? 'VISIBLE' : 'NO VISIBLE',
                style: TextStyle(color: Colors.white)),
          ),
        ),
        DataCell(Row(
          children: [
            ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                primary: data[index].visible ? Colors.red : Colors.green,
              ),
              icon: Icon(data[index].visible ? Icons.delete : Icons.done),
              onPressed: () async {
                await showDialog(
                    context: context,
                    builder: (_) {
                      return AlertDialog(
                          title: Text(data[index].visible
                              ? "Desea poner como no visible?"
                              : "Desea poner como visible ?"),
                          content: Container(
                            height: 70,
                            child: Column(
                              children: [
                                Text(data[index].visible
                                    ? "Confirme para poner video como no visible"
                                    : "Confirme para poner video como visible"),
                                SizedBox(
                                  height: 20,
                                ),
                                ElevatedButton.icon(
                                    icon: Icon(data[index].visible
                                        ? Icons.delete
                                        : Icons.done),
                                    style: ElevatedButton.styleFrom(
                                        primary: data[index].visible
                                            ? Colors.red
                                            : Colors.green),
                                    onPressed: () async {
                                      EmergencyNumberFunction
                                          emergencyNumberFunction =
                                          EmergencyNumberFunction();

                                      Map<String, dynamic> user = {
                                        'visible': !data[index].visible
                                      };

                                      await emergencyNumberFunction
                                          .editEmergencyNumber(
                                              user, data[index].id);

                                      Navigator.pop(context);
                                    },
                                    label: Text(data[index].visible
                                        ? "No visible"
                                        : "Visible"))
                              ],
                            ),
                          ));
                    }).then((value) {
                  refresh();
                });
              },
              label: Text(data[index].visible ? "No visible" : 'Visible'),
            ),
            // DataCell(Row(
            //   children: [
            //     ElevatedButton.icon(
            //       style: ElevatedButton.styleFrom(
            //         primary: Colors.red,
            //       ),
            //       icon: Icon(Icons.delete),
            //       onPressed: () {
            //         showDialog(
            //             context: context,
            //             builder: (_) {
            //               return AlertDialog(
            //                   title: Text("Desea eliminar ?"),
            //                   content: Container(
            //                     height: 70,
            //                     child: Column(
            //                       children: [
            //                         Text(
            //                             "Confirme para eliminar número de teléfono"),
            //                         SizedBox(
            //                           height: 20,
            //                         ),
            //                         ElevatedButton.icon(
            //                             icon: Icon(Icons.delete),
            //                             style: ElevatedButton.styleFrom(
            //                                 primary: Colors.red),
            //                             onPressed: () async {
            //                               await emergencyNumberFunction
            //                                   .deleteEmergencyNumber(
            //                                       data[index].id);
            //                               refresh();
            //                               Navigator.pop(context);
            //                             },
            //                             label: Text(
            //                                 "Confirmar eliminación de número de emergencia"))
            //                       ],
            //                     ),
            //                   ));
            //             });
            //       },
            //       label: Text("Eliminar"),
            //     ),
            SizedBox(
              width: 6,
            ),
            ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                primary: Colors.blue,
              ),
              icon: Icon(Icons.edit),
              onPressed: () async {
                await showDialog(
                    context: context,
                    builder: (_) {
                      return AlertDialog(
                        title: Text("Editar número de emergencia"),
                        content: Container(
                            child: EmergencyNumbereoFormWidget(
                          id: data[index].id,
                          telefono: data[index].telefono,
                          nombre: data[index].nombre,
                        )),
                      );
                    }).then((value) {
                  refresh();
                });
              },
              label: Text("Editar"),
            ),
          ],
        )),
      ],
    );
  }

  @override
  int get selectedRowCount {
    return 0;
  }

  @override
  bool get isRowCountApproximate {
    return false;
  }

  @override
  int get rowCount {
    return data.length;
  }
}
