import 'package:flutter/material.dart';
import 'package:flutter_2/app/firebase/user.function.dart';
import 'package:flutter_2/app/models/user.dart';

class CaretakersTab extends StatefulWidget {
  const CaretakersTab({Key key}) : super(key: key);

  @override
  _CaretakersTabState createState() => _CaretakersTabState();
}

class _CaretakersTabState extends State<CaretakersTab>
    with TickerProviderStateMixin {
  UsersFunction usersFunction = UsersFunction();
  int _rowsPerPage = 15;
  TextEditingController _textEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: ListView(children: [
              FutureBuilder<List<Usuario>>(
                future: usersFunction.getUsers(),
                builder:
                    (BuildContext context, AsyncSnapshot<List<Usuario>> data) {
                  if (data.hasData) {
                    data.data
                        .where((element) => element.rol == 'cuidador')
                        .toList()
                        .sort((a, b) =>
                            a.nombreAdultoMayor.compareTo(b.nombreAdultoMayor));
                    return Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            controller: _textEditingController,
                            decoration: InputDecoration(
                                hintText:
                                    'Busca por Correo electrónico/Nombre adulto mayor'),
                            onChanged: (String email) => setState(() {}),
                          ),
                        ),
                        PaginatedDataTable(
                            rowsPerPage: _rowsPerPage,
                            onRowsPerPageChanged: (v) {
                              setState(() {
                                _rowsPerPage = v;
                              });
                            },
                            availableRowsPerPage: [15, 30, 45, 60],
                            header: Text("Gestión de cuidadores"),
                            // actions: [
                            //   IconButton(icon: Icon(Icons.add), onPressed: () {}),
                            //   IconButton(icon: Icon(Icons.search), onPressed: () {})
                            // ],
                            showCheckboxColumn: true,
                            sortAscending: true,
                            columns: <DataColumn>[
                              DataColumn(label: Text("Correo electrónico")),
                              DataColumn(label: Text("Nombre adulto mayor")),
                              DataColumn(label: Text("Habilitado")),
                              DataColumn(label: Text("Acciones")),
                            ],
                            source: MyDataTableSource(
                                data.data
                                    .where((element) =>
                                        element.rol == 'cuidador' &&
                                        (element.correoElectronico
                                                .toLowerCase()
                                                .contains(_textEditingController
                                                    .text
                                                    .toLowerCase()) ||
                                            element.nombreAdultoMayor
                                                .toLowerCase()
                                                .contains(_textEditingController
                                                    .text
                                                    .toLowerCase())))
                                    .toList(),
                                context, () {
                              setState(() {});
                            })),
                      ],
                    );
                  }
                  return LinearProgressIndicator(
                    backgroundColor: Colors.transparent,
                    valueColor:
                        new AlwaysStoppedAnimation<Color>(Color(0xFF1e282c)),
                  );
                },
              ),
            ]),
          )
        ],
      ),
      // floatingActionButton: FloatingActionButton(
      //   backgroundColor: kPrimaryColor,
      //   onPressed: () {},
      //   child: Icon(Icons.add),
      // ),
    );
  }
}

class MyDataTableSource extends DataTableSource {
  MyDataTableSource(this.data, this.context, this.refresh);

  final List<dynamic> data;
  BuildContext context;
  Function refresh;

  @override
  DataRow getRow(int index) {
    if (index >= data.length) {
      return null;
    }
    return DataRow.byIndex(
      index: index,
      cells: [
        DataCell(Text('${data[index].correoElectronico}')),
        DataCell(Text('${data[index].nombreAdultoMayor}')),
        DataCell(
          Chip(
            padding: EdgeInsets.all(0),
            backgroundColor: data[index].habilitado ? Colors.green : Colors.red,
            label: Text(data[index].habilitado ? 'HABILITADO' : 'DESHABILITADO',
                style: TextStyle(color: Colors.white)),
          ),
        ),
        DataCell(Row(
          children: [
            ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                primary: data[index].habilitado ? Colors.green : Colors.red,
              ),
              icon: Icon(data[index].habilitado ? Icons.delete : Icons.done),
              onPressed: () async {
                await showDialog(
                    context: context,
                    builder: (_) {
                      return AlertDialog(
                          title: Text(data[index].habilitado
                              ? "Desea deshabilitar?"
                              : "Desea habilitar ?"),
                          content: Container(
                            height: 70,
                            child: Column(
                              children: [
                                Text(data[index].habilitado
                                    ? "Confirme para deshabilitar cuidador"
                                    : "Confirme para habilitar cuidador"),
                                SizedBox(
                                  height: 20,
                                ),
                                ElevatedButton.icon(
                                    icon: Icon(data[index].habilitado
                                        ? Icons.delete
                                        : Icons.done),
                                    style: ElevatedButton.styleFrom(
                                        primary: data[index].habilitado
                                            ? Colors.red
                                            : Colors.green),
                                    onPressed: () async {
                                      UsersFunction usersFunction =
                                          UsersFunction();

                                      Map<String, dynamic> user = {
                                        'habilitado': !data[index].habilitado
                                      };

                                      await usersFunction.edituser(
                                          user, data[index].id);

                                      Navigator.pop(context);
                                    },
                                    label: Text(data[index].habilitado
                                        ? "Deshabilitar cuidador"
                                        : "Habilitar cuidador"))
                              ],
                            ),
                          ));
                    }).then((value) {
                  refresh();
                });
              },
              label:
                  Text(data[index].habilitado ? "Deshabilitar" : 'Habilitar'),
            ),
            // SizedBox(
            //   width: 6,
            // ),
            // ElevatedButton.icon(
            //   style: ElevatedButton.styleFrom(
            //     primary: Colors.blue,
            //   ),
            //   icon: Icon(Icons.edit),
            //   onPressed: () {},
            //   label: Text("Editar"),
            // ),
            // SizedBox(
            //   width: 6,
            // ),
            // ElevatedButton.icon(
            //   style: ElevatedButton.styleFrom(
            //     primary: Colors.green,
            //   ),
            //   icon: Icon(Icons.visibility),
            //   onPressed: () {},
            //   label: Text("Ver"),
            // ),
          ],
        )),
      ],
    );
  }

  @override
  int get selectedRowCount {
    return 0;
  }

  @override
  bool get isRowCountApproximate {
    return false;
  }

  @override
  int get rowCount {
    return data.length;
  }
}
