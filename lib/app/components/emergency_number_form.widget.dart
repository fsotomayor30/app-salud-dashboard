import 'package:flutter/material.dart';
import 'package:flutter_2/app/firebase/emergency_number.function.dart';

class EmergencyNumbereoFormWidget extends StatefulWidget {
  final String nombre;
  final String telefono;
  final String id;

  EmergencyNumbereoFormWidget(
      {@required this.nombre, @required this.telefono, @required this.id});

  @override
  _EmergencyNumbereoFormWidgetState createState() =>
      _EmergencyNumbereoFormWidgetState();
}

class _EmergencyNumbereoFormWidgetState
    extends State<EmergencyNumbereoFormWidget> {
  EmergencyNumberFunction emergencyNumberFunction = EmergencyNumberFunction();
  bool loading = false;

  String nombre = '';
  String telefono = '';

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    setState(() {
      nombre = widget.nombre;
      telefono = widget.telefono;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return loading
        ? LinearProgressIndicator(
            backgroundColor: Colors.transparent,
            valueColor: new AlwaysStoppedAnimation<Color>(Color(0xFF1e282c)),
          )
        : Padding(
            padding: const EdgeInsets.all(20),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  TextFormField(
                    initialValue: nombre,
                    maxLength: 50,
                    decoration: InputDecoration(
                      labelText: 'Nombre:',
                      counterText: 'Nombre del número de emergencia',
                    ),
                    keyboardType: TextInputType.text,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Debes ingresar nombre del número de emergencia';
                      }
                      return null;
                    },
                    onChanged: (value) {
                      setState(() {
                        nombre = value;
                      });
                    },
                  ),
                  TextFormField(
                    initialValue: telefono,
                    maxLength: 50,
                    decoration: InputDecoration(
                      labelText: 'Teléfono:',
                      counterText: 'Teléfono del número de emergencia',
                    ),
                    keyboardType: TextInputType.text,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Debes ingresar teléfono del número de emergencia';
                      }
                      return null;
                    },
                    onChanged: (value) {
                      setState(() {
                        telefono = value;
                      });
                    },
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 8, top: 8, bottom: 8),
                        child: ElevatedButton.icon(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.green,
                          ),
                          icon: Icon(Icons.west),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          label: Text("Cerrar"),
                        ),
                      ),
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 8, top: 8, bottom: 8),
                        child: ElevatedButton.icon(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.blue,
                          ),
                          icon: Icon(Icons.edit),
                          onPressed: () async {
                            if (_formKey.currentState.validate()) {
                              setState(() {
                                loading = true;
                              });

                              if (widget.id == null) {
                                Map<String, dynamic> emergencyNumber = {
                                  'nombre': nombre,
                                  'telefono': telefono,
                                  'user': '',
                                  'all_user': true,
                                };
                                await emergencyNumberFunction
                                    .saveEmergencyNumber(emergencyNumber);
                              } else {
                                Map<String, dynamic> emergencyNumber = {
                                  'nombre': nombre,
                                  'telefono': telefono,
                                };
                                await emergencyNumberFunction
                                    .editEmergencyNumber(
                                        emergencyNumber, widget.id);
                              }

                              setState(() {
                                loading = false;
                              });

                              Navigator.pop(context);
                            }
                          },
                          label: Text(widget.id == null ? 'Guardar' : "Editar"),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          );
  }
}
