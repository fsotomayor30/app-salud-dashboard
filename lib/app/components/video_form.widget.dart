import 'package:flutter/material.dart';
import 'package:flutter_2/app/firebase/video.function.dart';

class VideoFormWidget extends StatefulWidget {
  final String url;
  final String titulo;
  final String duracion;
  final String tipo;
  final String id;

  VideoFormWidget(
      {@required this.url,
      @required this.tipo,
      @required this.duracion,
      @required this.titulo,
      @required this.id});

  @override
  _VideoFormWidgetState createState() => _VideoFormWidgetState();
}

class _VideoFormWidgetState extends State<VideoFormWidget> {
  VideoFunction videoFunction = VideoFunction();
  bool loading = false;

  String url = '';
  String titulo = '';
  String duracion = '';
  String tipo = '';

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    setState(() {
      url = widget.url;
      titulo = widget.titulo;
      duracion = widget.duracion;
      tipo = widget.tipo.isEmpty ? 'Técnica de insulinoterapia' : widget.tipo;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return loading
        ? LinearProgressIndicator(
            backgroundColor: Colors.transparent,
            valueColor: new AlwaysStoppedAnimation<Color>(Color(0xFF1e282c)),
          )
        : Padding(
            padding: const EdgeInsets.all(20),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  TextFormField(
                    initialValue: url,
                    maxLength: 50,
                    decoration: InputDecoration(
                      labelText: 'Url:',
                      counterText: 'Link del video de Youtube',
                    ),
                    keyboardType: TextInputType.text,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Debes ingresar url';
                      }
                      return null;
                    },
                    onChanged: (value) {
                      setState(() {
                        url = value;
                      });
                    },
                  ),
                  TextFormField(
                    initialValue: titulo,
                    maxLength: 50,
                    decoration: InputDecoration(
                      labelText: 'Título:',
                      counterText: 'Título del video de Youtube',
                    ),
                    keyboardType: TextInputType.text,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Debes ingresar título del video';
                      }
                      return null;
                    },
                    onChanged: (value) {
                      setState(() {
                        titulo = value;
                      });
                    },
                  ),
                  TextFormField(
                    initialValue: duracion,
                    maxLength: 50,
                    decoration: InputDecoration(
                      labelText: 'Duración de video:',
                      counterText: 'Duración del video de Youtube',
                    ),
                    keyboardType: TextInputType.text,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Debes ingresar duración de video';
                      }
                      return null;
                    },
                    onChanged: (value) {
                      setState(() {
                        duracion = value;
                      });
                    },
                  ),
                  DropdownButton<String>(
                    isExpanded: true,
                    value: tipo,
                    //elevation: 5,
                    style: TextStyle(color: Colors.black),
                    items: <String>[
                      'Técnica de insulinoterapia',
                      'Técnica de toma de glicemia',
                      'Técnica de toma de presión digital',
                      'Técnica de toma de presión manual',
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    hint: Text(
                      "Categoría de video:",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w600),
                    ),
                    onChanged: (String value) {
                      setState(() {
                        tipo = value;
                      });
                    },
                  ),
                  // TextFormField(
                  //   initialValue: tipo,
                  //   maxLength: 50,
                  //   decoration: InputDecoration(
                  //     labelText: 'Tipo de video:',
                  //     counterText: '',
                  //   ),
                  //   keyboardType: TextInputType.text,
                  //   validator: (value) {
                  //     if (value == null || value.isEmpty) {
                  //       return 'Debes ingresar tipo de video';
                  //     }
                  //     return null;
                  //   },
                  //   onChanged: (value) {
                  //     setState(() {
                  //       tipo = value;
                  //     });
                  //   },
                  // ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 8, top: 8, bottom: 8),
                        child: ElevatedButton.icon(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.green,
                          ),
                          icon: Icon(Icons.west),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          label: Text("Cerrar"),
                        ),
                      ),
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 8, top: 8, bottom: 8),
                        child: ElevatedButton.icon(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.blue,
                          ),
                          icon: Icon(Icons.edit),
                          onPressed: () async {
                            if (_formKey.currentState.validate()) {
                              setState(() {
                                loading = true;
                              });

                              Map<String, dynamic> video = {
                                'duracion': duracion,
                                'tipo': tipo,
                                'titulo': titulo,
                                'url': url,
                                'visible': true
                              };

                              if (widget.id == null) {
                                await videoFunction.saveVideo(video);
                              } else {
                                await videoFunction.editVideo(video, widget.id);
                              }

                              setState(() {
                                loading = false;
                              });

                              Navigator.pop(context);
                            }
                          },
                          label: Text(widget.id == null ? 'Guardar' : "Editar"),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          );
  }
}
