import 'package:flutter/material.dart';
import 'package:flutter_2/app/components/historial_item.widget.dart';
import 'package:flutter_2/app/components/mood_item.widget.dart';
import 'package:flutter_2/app/constant/constants.dart';
import 'package:flutter_2/app/firebase/user.function.dart';
import 'package:flutter_2/app/models/measurement.dart';
import 'package:flutter_2/app/models/mood.dart';
import 'package:flutter_2/app/firebase/measurement.function.dart';
import 'package:flutter_2/app/firebase/mood.function.dart';
import 'package:flutter_2/app/models/user.dart';

class HistorialWidget extends StatefulWidget {
  final String email;

  HistorialWidget({
    @required this.email,
  });

  @override
  _HistorialWidgetState createState() => _HistorialWidgetState();
}

class _HistorialWidgetState extends State<HistorialWidget> {
  MeasurementFunction measurementFunction = MeasurementFunction();
  MoodFunction moodFunction = MoodFunction();
  UsersFunction usersFunction = UsersFunction();

  bool loadingMeasurement = true;
  bool loadingMood = true;
  bool loadingUser = true;

  List<Measurement> measurements = <Measurement>[];
  List<Mood> mods = <Mood>[];
  String usuario;

  @override
  void initState() {
    super.initState();
    loadMeasurement();
    loadMood();
    loadUser();
  }

  loadUser() async {
    usersFunction.getUserByEmail(widget.email).then((value) {
      setState(() {
        usuario = value['nombres'] +
            ' ' +
            value['apellido_paterno'] +
            ' ' +
            value['apellido_materno'];
        loadingUser = false;
      });
    });
  }

  loadMeasurement() async {
    measurementFunction.getSymptomsByUser(widget.email).then((value) {
      setState(() {
        measurements = value;
        loadingMeasurement = false;
      });
    });
  }

  loadMood() async {
    moodFunction.getMoodByUser(widget.email).then((value) {
      setState(() {
        mods = value;
        loadingMood = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return loadingMeasurement || loadingMood || loadingUser
        ? LinearProgressIndicator(
            backgroundColor: Colors.transparent,
            valueColor: new AlwaysStoppedAnimation<Color>(Color(0xFF1e282c)),
          )
        : Container(
            width: size.width * .5,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 15),
                  child: Text('Nombre adulto mayor: ' + usuario,
                      textAlign: TextAlign.start,
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),
                ),
                Expanded(
                  child: DefaultTabController(
                    length: 2,
                    child: MaterialApp(
                      debugShowCheckedModeBanner: false,
                      home: Scaffold(
                        appBar: new PreferredSize(
                          preferredSize: Size.fromHeight(kToolbarHeight),
                          child: new Container(
                            color: kPrimaryColor,
                            child: new SafeArea(
                              child: Column(
                                children: <Widget>[
                                  new Expanded(child: new Container()),
                                  TabBar(
                                    // labelColor: Colors.black,
                                    indicatorColor: Colors.white,
                                    onTap: (index) {
                                      // Tab index when user select it, it start from zero
                                    },
                                    tabs: [
                                      Tab(text: 'Mediciones diarias'),
                                      Tab(
                                        text: 'Estados de animo',
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),

                        // appBar: AppBar(
                        //   backgroundColor: Colors.white,
                        //   elevation: 0,
                        //   bottom: ,
                        //   // title: Text('Tabs Demo'),
                        // ),
                        body: TabBarView(
                          children: [
                            ListView(
                                children: measurements
                                    .map((e) => HistoryItemWidget(
                                          measurement: e,
                                        ))
                                    .toList()),
                            ListView(
                                children: mods
                                    .map((e) => MoodItemWidget(
                                          mood: e,
                                        ))
                                    .toList()),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
  }
}
