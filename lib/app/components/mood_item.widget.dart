import 'package:flutter/material.dart';
import 'package:flutter_2/app/constant/constants.dart';
import 'package:flutter_2/app/models/mood.dart';

class MoodItemWidget extends StatefulWidget {
  final Mood mood;

  MoodItemWidget({@required this.mood});

  @override
  _MoodItemWidgetState createState() => _MoodItemWidgetState();
}

class _MoodItemWidgetState extends State<MoodItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          border: Border.all(
            width: 1,
            color: kPrimaryColor,
          ),
        ),
        child: Column(
          children: [
            Container(
              // width: size.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10.0),
                    topLeft: Radius.circular(10.0)),
                color: kPrimaryColor,
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Registro de estados de ánimo',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 5),
                    Row(
                      children: [
                        Icon(
                          Icons.access_time,
                          color: Colors.white,
                          size: 15,
                        ),
                        SizedBox(width: 5),
                        Text(
                          'Con fecha de ' + widget.mood.fechaRegistro,
                          style: TextStyle(color: Colors.white),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text('Tu estado de ánimo registrado es: ' +
                          widget.mood.estadoAnimo),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
