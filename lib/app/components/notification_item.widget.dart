import 'package:flutter/material.dart';
import 'package:flutter_2/app/components/historial.widget.dart';
import 'package:flutter_2/app/constant/constants.dart';

class NotificationItemWidget extends StatefulWidget {
  final String title;
  final String body;
  final String fecha;
  final String correoElectronico;
  final TabController tabController;
  final BuildContext newContext;

  NotificationItemWidget(
      {@required this.title,
      @required this.body,
      @required this.fecha,
      @required this.correoElectronico,
      @required this.tabController,
      @required this.newContext});

  @override
  _NotificationItemWidgetState createState() => _NotificationItemWidgetState();
}

class _NotificationItemWidgetState extends State<NotificationItemWidget> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: InkWell(
        onTap: () async {
          widget.tabController.animateTo(1);
          await showDialog(
              context: widget.newContext,
              builder: (_) {
                return AlertDialog(
                  content: HistorialWidget(email: widget.correoElectronico),
                  actions: [
                    ElevatedButton.icon(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.green,
                      ),
                      icon: Icon(Icons.west),
                      onPressed: () {
                        Navigator.pop(widget.newContext);
                      },
                      label: Text("Cerrar"),
                    ),
                  ],
                );
              });
        },
        child: Container(
          width: size.width * .2,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            border: Border.all(
              width: 1,
              color: kPrimaryColor,
            ),
          ),
          child: Column(
            children: [
              Container(
                width: size.width * .2,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(10.0),
                      topLeft: Radius.circular(10.0)),
                  color: kPrimaryColor,
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.title,
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 5),
                      Row(
                        children: [
                          Icon(
                            Icons.access_time,
                            size: 15,
                            color: Colors.white,
                          ),
                          SizedBox(width: 5),
                          Text(
                            'Fecha: ' + widget.fecha,
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [Text(widget.body)],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
