import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_2/app/firebase/user.function.dart';
import 'package:flutter_2/app/service_auth/auth.service.dart';
import 'package:fluttertoast/fluttertoast.dart';

class AdminFormWidget extends StatefulWidget {
  final String correoElectronico;
  final String id;

  AdminFormWidget({@required this.correoElectronico, @required this.id});

  @override
  _AdminFormWidgetState createState() => _AdminFormWidgetState();
}

class _AdminFormWidgetState extends State<AdminFormWidget> {
  UsersFunction usersFunction = UsersFunction();
  final AuthService authService = AuthService();
  bool loading = false;

  String correoElectronico = '';
  String password = '';

  final _formKey = GlobalKey<FormState>();

  FToast fToast;

  @override
  void initState() {
    fToast = FToast();
    fToast.init(context);

    setState(() {
      correoElectronico = widget.correoElectronico;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return loading
        ? LinearProgressIndicator(
            backgroundColor: Colors.transparent,
            valueColor: new AlwaysStoppedAnimation<Color>(Color(0xFF1e282c)),
          )
        : Padding(
            padding: const EdgeInsets.all(20),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  TextFormField(
                    initialValue: correoElectronico,
                    maxLength: 50,
                    decoration: InputDecoration(
                      labelText: 'Correo electrónico:',
                      counterText: 'Correo electrónico',
                    ),
                    keyboardType: TextInputType.text,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Debes ingresar correo electrónico';
                      }
                      return null;
                    },
                    onChanged: (value) {
                      setState(() {
                        correoElectronico = value;
                      });
                    },
                  ),
                  widget.id == null
                      ? TextFormField(
                          initialValue: correoElectronico,
                          maxLength: 50,
                          decoration: InputDecoration(
                            labelText: 'Contraseña:',
                            counterText: 'Contraseña del administrador',
                          ),
                          keyboardType: TextInputType.text,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Debes ingresar contraseña del administrador';
                            }
                            return null;
                          },
                          onChanged: (value) {
                            setState(() {
                              password = value;
                            });
                          },
                        )
                      : Container(),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 8, top: 8, bottom: 8),
                        child: ElevatedButton.icon(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.green,
                          ),
                          icon: Icon(Icons.west),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          label: Text("Cerrar"),
                        ),
                      ),
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 8, top: 8, bottom: 8),
                        child: ElevatedButton.icon(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.blue,
                          ),
                          icon: Icon(Icons.edit),
                          onPressed: () async {
                            if (_formKey.currentState.validate()) {
                              setState(() {
                                loading = true;
                              });

                              if (widget.id == null) {
                                Map<String, dynamic> admin = {
                                  'correo_electronico': correoElectronico,
                                  'habilitado': true,
                                  'rol': 'admin'
                                };
                                dynamic register = await authService
                                    .createUserWithEmailAndPassword(
                                        email: correoElectronico,
                                        password: password);

                                if (register ==
                                    'The password provided is too weak.') {
                                  setState(() {
                                    loading = false;
                                  });
                                  _showToast(Colors.red, 'Contraseña muy débil',
                                      Icons.close);
                                  return;
                                }

                                if (register ==
                                    'The account already exists for that email.') {
                                  setState(() {
                                    loading = false;
                                  });
                                  _showToast(Colors.red, 'Correo existente',
                                      Icons.close);
                                  return;
                                }

                                if (register == null) {
                                  setState(() {
                                    loading = false;
                                  });
                                  _showToast(Colors.red,
                                      'Ha ocurrido un error!', Icons.close);

                                  return;
                                }

                                await usersFunction.saveUser(admin);
                              } else {
                                Map<String, dynamic> admin = {
                                  'correo_electronico': correoElectronico,
                                };
                                QueryDocumentSnapshot<Object> queryUser =
                                    await usersFunction
                                        .getUserByEmail(correoElectronico);

                                if (queryUser == null) {
                                  await usersFunction.edituser(
                                      admin, widget.id);
                                } else {
                                  setState(() {
                                    loading = false;
                                  });
                                  _showToast(Colors.red, 'Correo existente!',
                                      Icons.close);

                                  return;
                                }
                              }

                              setState(() {
                                loading = false;
                              });

                              Navigator.pop(context);
                            }
                          },
                          label: Text(widget.id == null ? 'Guardar' : "Editar"),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          );
  }

  _showToast(Color color, String text, IconData icon) {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: color,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(icon),
          SizedBox(
            width: 12.0,
          ),
          Text(text),
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.BOTTOM,
      toastDuration: Duration(seconds: 2),
    );

    // Custom Toast Position
    // fToast.showToast(
    //     child: toast,
    //     toastDuration: Duration(seconds: 2),
    //     positionedToastBuilder: (context, child) {
    //       return Positioned(
    //         child: child,
    //         top: 16.0,
    //         left: 16.0,
    //       );
    //     });
  }
}
