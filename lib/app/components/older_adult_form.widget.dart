import 'package:flutter/material.dart';
import 'package:flutter_2/app/firebase/user.function.dart';

class OlderAdultFormWidget extends StatefulWidget {
  final String apellidoMaterno;
  final String apellidopaterno;
  final String direccion;
  final String genero;
  final String nombres;
  final String sector;
  final String id;

  OlderAdultFormWidget(
      {@required this.apellidoMaterno,
      @required this.apellidopaterno,
      @required this.direccion,
      @required this.genero,
      @required this.nombres,
      @required this.sector,
      @required this.id});

  @override
  _OlderAdultFormWidgetState createState() => _OlderAdultFormWidgetState();
}

class _OlderAdultFormWidgetState extends State<OlderAdultFormWidget> {
  UsersFunction usersFunction = UsersFunction();
  bool loading = false;

  String apellidoMaterno = '';
  String apellidopaterno = '';
  String direccion = '';
  String genero = '';
  String nombres = '';
  String sector = '';

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    setState(() {
      apellidoMaterno = widget.apellidoMaterno;
      apellidopaterno = widget.apellidopaterno;
      direccion = widget.direccion;
      genero = widget.genero;
      nombres = widget.nombres;
      sector = widget.sector;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return loading
        ? LinearProgressIndicator(
            backgroundColor: Colors.transparent,
            valueColor: new AlwaysStoppedAnimation<Color>(Color(0xFF1e282c)),
          )
        : Padding(
            padding: const EdgeInsets.all(20),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  TextFormField(
                    initialValue: nombres,
                    maxLength: 50,
                    decoration: InputDecoration(
                      labelText: 'Nombres:',
                      counterText: 'Nombres del adulto mayor',
                    ),
                    keyboardType: TextInputType.text,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Debes ingresar el nombre del adulto mayor';
                      }
                      return null;
                    },
                    onChanged: (value) {
                      setState(() {
                        nombres = value;
                      });
                    },
                  ),
                  TextFormField(
                    initialValue: apellidopaterno,
                    maxLength: 50,
                    decoration: InputDecoration(
                      labelText: 'Apellido Paterno:',
                      counterText: 'Apellido paterno del adulto mayor',
                    ),
                    keyboardType: TextInputType.text,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Debes ingresar apellido paterno del adulto mayor';
                      }
                      return null;
                    },
                    onChanged: (value) {
                      setState(() {
                        apellidopaterno = value;
                      });
                    },
                  ),
                  TextFormField(
                    initialValue: apellidoMaterno,
                    maxLength: 50,
                    decoration: InputDecoration(
                      labelText: 'Apellido Materno:',
                      counterText: 'Apellido materno del adulto mayor',
                    ),
                    keyboardType: TextInputType.text,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Debes ingresar apellido materno del adulto mayor';
                      }
                      return null;
                    },
                    onChanged: (value) {
                      setState(() {
                        apellidoMaterno = value;
                      });
                    },
                  ),
                  TextFormField(
                    initialValue: direccion,
                    maxLength: 50,
                    decoration: InputDecoration(
                      labelText: 'Dirección:',
                      counterText: 'Dirección del adulto mayor',
                    ),
                    keyboardType: TextInputType.text,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Debes ingresar dirección del adulto mayor';
                      }
                      return null;
                    },
                    onChanged: (value) {
                      setState(() {
                        direccion = value;
                      });
                    },
                  ),
                  TextFormField(
                    initialValue: sector,
                    maxLength: 50,
                    decoration: InputDecoration(
                      labelText: 'Sector:',
                      counterText: 'Sector del adulto mayor',
                    ),
                    keyboardType: TextInputType.text,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Debes ingresar sector del adulto mayor';
                      }
                      return null;
                    },
                    onChanged: (value) {
                      setState(() {
                        sector = value;
                      });
                    },
                  ),
                  TextFormField(
                    initialValue: genero,
                    enabled: false,
                    maxLength: 50,
                    decoration: InputDecoration(
                      labelText: 'Género:',
                      counterText: 'Género del adulto mayor',
                    ),
                    keyboardType: TextInputType.text,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Debes ingresar género del adulto mayor';
                      }
                      return null;
                    },
                    onChanged: (value) {
                      setState(() {
                        genero = value;
                      });
                    },
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 8, top: 8, bottom: 8),
                        child: ElevatedButton.icon(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.green,
                          ),
                          icon: Icon(Icons.west),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          label: Text("Cerrar"),
                        ),
                      ),
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 8, top: 8, bottom: 8),
                        child: ElevatedButton.icon(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.blue,
                          ),
                          icon: Icon(Icons.edit),
                          onPressed: () async {
                            if (_formKey.currentState.validate()) {
                              setState(() {
                                loading = true;
                              });

                              Map<String, dynamic> user = {
                                'apellido_materno': apellidoMaterno,
                                'apellido_paterno': apellidopaterno,
                                'direccion_particular': direccion,
                                'nombres': nombres,
                                'sector': sector
                              };

                              await usersFunction.edituser(user, widget.id);

                              setState(() {
                                loading = false;
                              });

                              Navigator.pop(context);
                            }
                          },
                          label: Text("Editar"),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          );
  }
}
