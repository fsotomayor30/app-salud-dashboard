import 'package:flutter/material.dart';
import 'package:flutter_2/app/constant/constants.dart';
import 'package:flutter_2/app/models/measurement.dart';

class HistoryItemWidget extends StatefulWidget {
  final Measurement measurement;

  HistoryItemWidget({@required this.measurement});

  @override
  _HistoryItemWidgetState createState() => _HistoryItemWidgetState();
}

class _HistoryItemWidgetState extends State<HistoryItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          border: Border.all(
            width: 1,
            color: kPrimaryColor,
          ),
        ),
        child: Column(
          children: [
            Container(
              // width: size.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10.0),
                    topLeft: Radius.circular(10.0)),
                color: kPrimaryColor,
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Registro de sintomas',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 5),
                    Row(
                      children: [
                        Icon(
                          Icons.access_time,
                          color: Colors.white,
                          size: 15,
                        ),
                        SizedBox(width: 5),
                        Text(
                          'Con fecha de ' + widget.measurement.fechaRegistro,
                          style: TextStyle(color: Colors.white),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text('Glicemia: ' +
                          widget.measurement.glicemia +
                          ' mg/DL'),
                    ],
                  ),
                  Text('Colesterol: ' + widget.measurement.colesterol),
                  Text('Presión: ' +
                      widget.measurement.presionSistolica +
                      '/' +
                      widget.measurement.presionDiastolica +
                      ' mmHg')
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
